import React, { useEffect, useState } from 'react'
import GetAllDescriptionies from './services/getAllDescriptionies.js'
import GetAllCatergories from './services/getAllCatergories'
import 'bootstrap/dist/css/bootstrap.min.css'
import Button from 'react-bootstrap/Button'
import SaveIcon from '@material-ui/icons/Save';

export default function EditParcelForm(props) {
    const initialFormState = { parcelNumber: 0, parcelName: '', descriptionKey: 0, description: '', catergoryKey: 0, name: '', parcelWeight: 0, parcelWidth: 0, height: 0 }
    const [parcel, setParcel] = useState(props.currentParcel)
    const [descriptionies, setDescriptionies] = useState([])
    const [catergories, setCatergories] = useState([])
    const descriptionData = GetAllDescriptionies()
    const catergoryData = GetAllCatergories()
    useEffect(() => {
        setDescriptionies(descriptionData)
        setCatergories(catergoryData)
    }, [descriptionData, catergoryData])


    const handleInputChange = event => {
        const { name, value } = event.target;
        setParcel({ ...parcel, [name]: value })
    }

    const handleCancel = event => {
        props.closeModal()
        props.setEditing(false)
    }
    return (
        <form
            onSubmit={(event) => {
                event.preventDefault()
                setParcel(initialFormState)
            }}
        >
            <label>Name</label>
            <input
                type="text"
                name="parcelName"
                value={parcel.parcelName}
                onChange={handleInputChange}
            />
            <label>
                Description:
                <select name="descriptionKey" value={parcel.descriptionKey} onChange={handleInputChange}>
                    <option value="0">Select description</option>
                    {
                        descriptionies.map((description) => (
                            <option value={description.descriptionKey}>{description.description}</option>
                        ))
                    }
                </select>
            </label>
            <label>
                Catergory :
                <select name="catergoryKey" value={parcel.catergoryKey} onChange={handleInputChange}>
                    <option value="0">Select catergory</option>
                    {
                        catergories.map((catergory) => (
                            <option value={catergory.catergoryKey}>{catergory.name}</option>
                        ))
                    }
                </select>
            </label>

            <label>Weight</label>
            <input
                type="number"
                name="parcelWeight"
                value={parcel.parcelWeight}
                onChange={handleInputChange}
            />
            <label>Width</label>
            <input
                type="number"
                name="parcelWidth"
                value={parcel.parcelWidth}
                onChange={handleInputChange}
            />
            <label>Height</label>
            <input
                type="number"
                name="height"
                value={parcel.height}
                onChange={handleInputChange}
            />
            <Button
                variant="primary"
                onClick={(event) => {
                    catergories
                        .filter(catergory => parseInt(catergory.catergoryKey) === parseInt(parcel.catergoryKey))
                        .map(catergory =>
                            parcel.name = catergory.name,
                            setParcel({ ...parcel })
                        )
                    descriptionies
                        .filter(description => parseInt(description.descriptionKey) === parseInt(parcel.descriptionKey))
                        .map((description) =>
                            parcel.description = description.description,
                            setParcel({ ...parcel })
                        )
                    props.updateParcel(parcel)
                    event.preventDefault()
                    setParcel(initialFormState)
                }}><SaveIcon />Update parcel</Button>
            <Button
                style={{
                    position: 'absolute',
                    right: 15
                }}
                variant="danger"
                onClick={handleCancel}>
                Cancel
            </Button>
        </form>
    )
}