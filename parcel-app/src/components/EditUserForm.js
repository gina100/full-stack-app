import React, { useState } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css'
import Button from 'react-bootstrap/Button'
import SaveIcon from '@material-ui/icons/Save';
import CancelIcon from '@material-ui/icons/Cancel';

export default function EditUserForm(props) {
    const [user, setUser] = useState(props.currentUser)
    const [address, setAddress] = useState(props.currentAddress)

    const handleUserInputChange = event => {
        const { name, value } = event.target;
        setUser({ ...user, [name]: value })
    }
    const handleAddressInputChange = event => {
        const { name, value } = event.target;
        setAddress({ ...address, [name]: value })
    }

    return (
        <form
            onSubmit={(event) => {
                event.preventDefault()
            }}
        >
            <label>User Name</label>
            <input
                type="text"
                name="userName"
                value={user.userName}
                onChange={handleUserInputChange}
            />

            <label>First Name</label>
            <input
                type="text"
                name="firstName"
                value={user.firstName}
                onChange={handleUserInputChange}
            />

            <label>Email</label>
            <input
                type="text"
                name="email"
                value={user.email}
                onChange={handleUserInputChange}
            />

            <label>City Name</label>
            <input
                type="text"
                name="cityName"
                value={address.cityName}
                onChange={handleAddressInputChange}
            />

            <label>Address</label>
            <input
                type="text"
                name="address"
                value={address.address}
                onChange={handleAddressInputChange}
            />

            <label>Address Region</label>
            <input
                type="text"
                name="addressRegion"
                value={address.addressRegion}
                onChange={handleAddressInputChange}
            />

            <label>Address Postal Code</label>
            <input
                type="text"
                name="addressPostalCode"
                value={address.addressPostalCode}
                onChange={handleAddressInputChange}
            />
            <Button onClick={(event) => {
                props.updateUser(user, address)
                event.preventDefault()
            }}><SaveIcon />Update Account</Button>
            <Button
                onClick={() => props.closeModal()}
                style={{
                    position: 'absolute',
                    right: 15
                }}
                variant="danger"
            >
                <CancelIcon />   Cancel
            </Button>
        </form>
    )
}