import React, { useState } from 'react'
import '../index.css'
import useSortableData from './sort.js'
import './styles.css';
import "bootstrap/dist/css/bootstrap.min.css";
import Table from "react-bootstrap/Table";
import Loader from 'react-loader-spinner';
import Alert from 'react-bootstrap/Alert'
import Button from 'react-bootstrap/Button'
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import Modal from 'react-bootstrap/Modal'
import CancelIcon from '@material-ui/icons/Cancel';
import ThumbUpIcon from '@material-ui/icons/ThumbUp';
import Pagination from 'react-bootstrap/Pagination'
import PageItem from 'react-bootstrap/PageItem'

const ParcelTable = (props) => {
    const [showDeleteModal, setShowDeleteModal] = useState(false)
    const [showDeleteAlert, setShowDeleteAlert] = useState(false)
    const [parcel, setParcel] = useState([])
    const { items, requestSort, sortConfig } = useSortableData(props.parcels);
    const getClassNamesFor = (name) => {
        if (!sortConfig) {
            return;
        }
        return sortConfig.key === name ? sortConfig.direction : undefined;
    };

    const deleteModal = (parcel) => {
        setParcel(parcel)
        setShowDeleteModal(true)
    }
    const handleClose = () => setShowDeleteModal(false);
    const handleDelete = () => {
        setShowDeleteModal(false);
        props.deleteParcel(parcel.parcelNumber)
        setShowDeleteAlert(true)
        window.setTimeout(() => {
            setShowDeleteAlert(false)
        }, 2000)
    }
    return ((

        <div>
            <>
                <Alert variant="success" show={showDeleteAlert} onClose={() => setShowDeleteAlert(false)} dismissible>
                    <div className="d-flex justify-content-center">The Parcel was deleted!</div>
                </Alert>
                <Modal size="sm" show={showDeleteModal} onHide={handleClose}>
                    <Modal.Header closeButton>
                        <Modal.Title>Are you sure you want to Delete the parcel</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>{parcel.parcelName}</Modal.Body>
                    <Modal.Footer>
                        <Button variant="danger" onClick={handleClose}>
                            <CancelIcon />  Cancel
                        </Button>
                        <Button variant="success" onClick={handleDelete}>
                            <ThumbUpIcon />  Yes
                        </Button>
                    </Modal.Footer>
                </Modal>
            </>
            <Table striped bordered hover variant="dark">
                <thead>
                    <tr>
                        <th><Button variant="dark" onClick={() => requestSort('parcelName')}
                            className={getClassNamesFor('parcelName')}><b>Name</b></Button></th>
                        <th>
                            <Button variant="dark" onClick={() => requestSort('description')}
                                className={getClassNamesFor('description')}><b>Description</b></Button>
                        </th>
                        <th><Button variant="dark" onClick={() => requestSort('catergory')}
                            className={getClassNamesFor('catergory')}><b>Catergory</b></Button></th>
                        <th>Weight</th>
                        <th>Width</th>
                        <th>Height</th>
                        <th>Actions</th>
                    </tr>
                </thead>{
                    props.parcels.length > 0 ? (
                        <tbody>
                            {
                                items.map((parcel) => (
                                    <tr key={parcel.parcelNumber}>
                                        <td>{parcel.parcelName}</td>
                                        <td>{parcel.description}</td>
                                        <td>{parcel.name}</td>
                                        <td>{parcel.parcelWeight}</td>
                                        <td>{parcel.parcelWidth}</td>
                                        <td>{parcel.height}</td>
                                        <td>
                                            <EditIcon onClick={() => props.editParcel(parcel)} />
                                            <DeleteIcon onClick={() => deleteModal(parcel)} />
                                        </td>
                                    </tr>
                                ))
                            }

                        </tbody>
                    ) : (
                        <div className="child-center">
                            <Loader type="ThreeDots" color="#2BAD60" height="60%" width="60%" />
                        </div>)}

            </Table>
            <Pagination>
                <Pagination.First />
                <Pagination.Prev />
                <Pagination.Item>{1}</Pagination.Item>
                <Pagination.Ellipsis />

                <Pagination.Item>{10}</Pagination.Item>
                <Pagination.Item>{11}</Pagination.Item>
                <Pagination.Item active>{12}</Pagination.Item>
                <Pagination.Item>{13}</Pagination.Item>
                <Pagination.Item disabled>{14}</Pagination.Item>

                <Pagination.Ellipsis />
                <Pagination.Item>{20}</Pagination.Item>
                <Pagination.Next />
                <Pagination.Last />
            </Pagination>
        </div >
    ))
}
export default ParcelTable