import { useEffect, useState } from 'react'
import parcel_app_config from '../parcel_app_config.json'
const axios = require('axios')

export default function GetAllCatergories() {
  const [catergories, getCatergories] = useState([])
  const fetchCatergories = () => {
    axios.get(parcel_app_config.SERVER_URL + '/catergory').then(res => {
      getCatergories(res.data)
    })
      .catch(error => console.error(`Error: ${error}`));
  }

  useEffect(() => {
    fetchCatergories()
  }, [])


  return catergories
}
