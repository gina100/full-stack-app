
import parcel_app_config from '../parcel_app_config.json'
const axios = require('axios')

export default function UpdateParcel(parcel) {

  const updateParcel = () => {
    axios.put(parcel_app_config.SERVER_URL + '/parcel', parcel)
      .catch(error => console.error(`Error: ${error}`));
  }
  console.log(parcel)
  updateParcel()
}
