import { useEffect, useState } from 'react'
import parcel_app_config from '../parcel_app_config.json'
const axios = require('axios')

export default function GetUserRolesNumber() {
  const [numberOfUserRoles, getNumberOfUserRoles] = useState([])
  const fetchNumberOfUserRoles = () => {
    axios.get(parcel_app_config.SERVER_URL + '/Users/GetUserRolesNumber').then(res => {
      getNumberOfUserRoles(res.data)
    })
      .catch(error => console.error(`Error: ${error}`));
  }

  useEffect(() => {
    fetchNumberOfUserRoles()
  }, [])

  return numberOfUserRoles
}
