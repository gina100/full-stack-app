import { useEffect, useState } from 'react'
import parcel_app_config from '../parcel_app_config.json'

const axios = require('axios')

export default function GetAllItems(token) {
  const [items, getItems] = useState([])
  const config = {
    headers: {
      Authorization: `Bearer ${token.token}`
    }
  };
  const fetchAllItems = () => {
    axios.get(parcel_app_config.SERVER_URL + '/Item', config).then(res => {
      getItems(res.data)
    })
      .catch(error => console.error(`Error: ${error}`));
  }

  useEffect(() => {
    fetchAllItems()
  }, [])
  return items
}
