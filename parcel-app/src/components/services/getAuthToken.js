import parcel_app_config from '../parcel_app_config.json'

const axios = require('axios')


export default async function GetAuthToken(credentials) {

  try {
    let res = await axios.post(parcel_app_config.SERVER_URL + '/auth/token', credentials)
    let data = res.data

    return data;
  }
  catch (error) {
    console.log(error)
    return 'error'
    //console.error(`Error code: ${error.response.status}`)
  }
}