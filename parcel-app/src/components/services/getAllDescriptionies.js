import { useEffect, useState } from 'react'
import parcel_app_config from '../parcel_app_config.json'
import axios from 'axios'

export default function GetAllDescriptionies() {
  const [descriptionies, getDescriptionies] = useState([])
  const fetchDescriptionies = () => {
    axios.get(parcel_app_config.SERVER_URL + '/parcelDescription')
      .then(res => {
        getDescriptionies(res.data)
      })
      .catch(error => console.error(`Error: ${error}`));
  }
  useEffect(() => {
    fetchDescriptionies()
  }, [])
  return descriptionies
}
