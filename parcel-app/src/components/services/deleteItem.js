import parcel_app_config from '../parcel_app_config.json'
const axios = require('axios')

export default function DeleteItem(itemKey) {
  const deleteItem = () => {
    axios.delete(parcel_app_config.SERVER_URL + '/item/?itemKey=' + itemKey)
      .then()
      .catch(error => console.error(`Error: ${error}`));
  }
  deleteItem()
}