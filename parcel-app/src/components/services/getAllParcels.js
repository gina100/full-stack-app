import { useEffect, useState } from 'react'
import parcel_app_config from '../parcel_app_config.json'

const axios = require('axios')

export default function GetAllParcels(token) {
  const [parcels, getparcels] = useState([])
  const config = {
    headers: {
      Authorization: `Bearer ${token.token}`
    }
  };
  const fetchAllParcels = () => {
    axios.get(parcel_app_config.SERVER_URL + '/parcel', config).then(res => {
      getparcels(res.data)
    })
      .catch(error => console.error(`Error: ${error}`));
  }

  useEffect(() => {
    fetchAllParcels()
  }, [])
  return parcels
}
