
import parcel_app_config from '../parcel_app_config.json'
const axios = require('axios')

export default function UpdateAddress(address) {

  const updateAddress = () => {
    axios.put(parcel_app_config.SERVER_URL + '/Addresses', address)
      .catch(error => console.error(`Error: ${error}`));
  }
  console.log(address)
  updateAddress()
}
