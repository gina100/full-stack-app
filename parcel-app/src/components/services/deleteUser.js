import parcel_app_config from '../parcel_app_config.json'
const axios = require('axios')

export default function DeleteUser(userKey) {
  const deleteUser = () => {
    axios.delete(parcel_app_config.SERVER_URL + '/users/?userKey=' + userKey)
      .then()
      .catch(error => console.error(`Error: ${error}`));
  }
  deleteUser()
}