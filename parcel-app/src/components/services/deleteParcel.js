import parcel_app_config from '../parcel_app_config.json'
const axios = require('axios')

export default function DeleteParcel(parcelNumber) {
  const deleteParcel = () => {
    axios.delete(parcel_app_config.SERVER_URL + '/parcel/?parcelNumber=' + parcelNumber)
      .then()
      .catch(error => console.error(`Error: ${error}`));
  }
  deleteParcel()
}