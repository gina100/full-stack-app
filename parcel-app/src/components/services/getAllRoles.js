import { useEffect, useState } from 'react'
import parcel_app_config from '../parcel_app_config.json'
const axios = require('axios')

export default function GetAllRoles() {
  const [roles, getRoles] = useState([])

  useEffect(() => {
    fetchRoles()
  }, [])
  const fetchRoles = () => {
    axios.get(parcel_app_config.SERVER_URL + '/Users/GetAllRoles').then(res => {
      getRoles(res.data)
    })
      .catch(error => console.error(`Error: ${error}`));
  }

  return roles
}
