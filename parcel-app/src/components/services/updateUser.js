
import parcel_app_config from '../parcel_app_config.json'
const axios = require('axios')

export default function UpdateUser(parcel) {

  const updateUser = () => {
    axios.put(parcel_app_config.SERVER_URL + '/Users', parcel)
      .catch(error => console.error(`Error: ${error}`));
  }
  console.log(parcel)
  updateUser()
}
