import { useEffect, useState } from 'react'
import parcel_app_config from '../parcel_app_config.json'
const axios = require('axios')

export default function GetAllAddresses() {
  const [addresses, getAddresses] = useState([])

  useEffect(() => {
    fetchAddresses()
  }, [])
  const fetchAddresses = () => {
    axios.get(parcel_app_config.SERVER_URL + '/addresses').then(res => {
      getAddresses(res.data)
    })
      .catch(error => console.error(`Error: ${error}`));
  }

  return addresses
}
