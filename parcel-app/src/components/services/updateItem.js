
import parcel_app_config from '../parcel_app_config.json'
const axios = require('axios')

export default function UpdateItem(item) {

  const updateItem = () => {
    axios.put(parcel_app_config.SERVER_URL + '/item', item)
      .catch(error => console.error(`Error: ${error}`));
  }
  updateItem()
}
