import { useEffect, useState } from 'react'
import parcel_app_config from '../parcel_app_config.json'
const axios = require('axios')

export default function GetAllUsers() {
  const [users, getUsers] = useState([])

  useEffect(() => {
    fetchAllUsers()
  }, [])
  const fetchAllUsers = () => {
    axios.get(parcel_app_config.SERVER_URL + '/Users').then(res => {
      getUsers(res.data)
    })
      .catch(error => console.error(`Error: ${error}`));
  }

  return users
}
