import React from 'react';
import {Container,Col,Form,FormGroup,Label,Input,Button} from 'reactstrap'
import { parcelService } from '../services/parcelService';
import CreatableSelect  from 'react-select/creatable';

export class AddParcel extends React.Component{
    constructor(props){
        super(props)

this.state=
    {
        parcelName:'',
        parcelDescription:'',
        catergory:'',
        parcelWeight:0.0,
        parcelWidth:0.0,
        height:0.0,parcels:[]
    }
    this.populateParcelData();
}
 ;
//this.onClick=this.onClick.bind(this);
handleChange= (e)=> {  
  this.setState({[e.target.name]:e.target.value});  
  }  
handleCatergoriesChange= (e)=> {
  if(e!=null){
    console.log(e.value);
  this.setState({[this.state.catergory]:e.value});  
  }
  else{
    this.setState({[this.state.catergory]:''});  
  }
  }  

 async AddParcel(){
    const parcel={
        ParcelName:this.state.parcelName,
        ParcelDescription:this.state.parcelDescription,
        catergory:this.state.catergory,
        parcelWeight:this.state.parcelWeight,
        parcelWidth:this.state.parcelWidth,
        height:this.state.height
    };

    await parcelService.addParcel(parcel);
}

catergories (){
  const ctArray=[];
  {
    this.state.parcels.map(parcels=>
    {ctArray.push({value:parcels.catergory,label:parcels.catergory,name:"parcelName"})}
)}
return ctArray
}

render() { 
    return (  
       <Container className="App">  
        <h4 className="PageHeading">Enter Parcel Details</h4>  
        <Form className="form">  
          <Col>  
            <FormGroup row>  
              <Label for="parcelName" sm={2}>Name</Label>  
              <Col sm={10}>  
                <Input type="text" name="parcelName" onChange={this.handleChange} value={this.state.parcelName} placeholder="Enter parcel Name" />  
              </Col>  
            </FormGroup>  
            <FormGroup row>  
              <Label for="parcelDescription" sm={2}>Description</Label>  
              <Col sm={10}>  
                <Input type="text" name="parcelDescription" onChange={this.handleChange} value={this.state.parcelDescription} placeholder="Enter Description" />  
              </Col>  
            </FormGroup>  
            <FormGroup row>  
              <Label for="catergory" sm={2}>Catergory</Label>  
              <Col sm={10}>  
              <CreatableSelect 
                isClearable
                onChange={this.handleCatergoriesChange}
                options={this.catergories()}
              />
              </Col>  
            </FormGroup>  
            <FormGroup row>  
              <Label for="parcelWeight" sm={2}>Weight</Label>  
              <Col sm={10}>  
                <Input type="text" name="parcelWeight" onChange={this.handleChange} value={this.state.parcelWeight} placeholder="Enter Weight" />  
              </Col>  
            </FormGroup>  
            <FormGroup row>  
              <Label for="parcelWidth" sm={2}>Width</Label>  
              <Col sm={10}>  
                <Input type="text" name="parcelWidth" onChange={this.handleChange} value={this.state.parcelWidth} placeholder="Enter Width" />  
              </Col>  
            </FormGroup>

            <FormGroup row>  
              <Label for="height" sm={2}>Height</Label>  
              <Col sm={10}>  
                <Input type="text" name="height" onChange={this.handleChange} value={this.state.height} placeholder="Enter Height" />  
              </Col>  
            </FormGroup>  
          </Col>  
          <Col>  
            <FormGroup row>  
              <Col sm={5}>  
              </Col>  
              <Col sm={1}>  
              <button type="button" onClick= {()=>{this.AddParcel()}} className="btn btn-success">Submit</button>  
              </Col>  
              <Col sm={1}>  
                <Button color="danger">Cancel</Button>{' '}  
              </Col>  
              <Col sm={5}>  
              </Col>  
            </FormGroup>  
          </Col>  
        </Form>  
      </Container>  
    );  
    }  

    async populateParcelData() {
      const data=await parcelService.getAllParcels();
      this.setState({ parcels:data});
        }

}

