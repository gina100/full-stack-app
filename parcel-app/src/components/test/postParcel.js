import axios from 'axios';
 
export const API = 'https://localhost:5001/api/parcel';
 
export const postParcel = async query => {
  const parcel={ 
    ParcelName : "Iphone 7 plus",
    ParcelDescription : "Smart Phone",
    Catergory : "Electronics",
    ParcelWeight : 0.86,
    ParcelWidth : 0.8,
    Height : 3.9
  };
  return await axios.post(API,parcel);
};
 
postParcel('react');