import axios from 'axios';
import parcelUrl from './services/serverUrl.js';
 
export const fetchParcel = async query => {
  return await axios.get(parcelUrl+'/parcel');
};
 
fetchParcel('react');