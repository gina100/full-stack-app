import React, { useState } from 'react';
import PropTypes from 'prop-types';
import './Login.css';
import GetAuthToken from '../services/getAuthToken';
import App from '../../App';
import Alert from 'react-bootstrap/Alert'
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.min.js';
import AppNav from '../../AppNav';
import ParcelApp from '../../ParcelApp';
import UserApp from '../../UserApp';
// import Login from '../src/components/Login/Login';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import ItemsApp from '../../ItemsApp';

export default function Login({ setToken }) {
    const [username, setUserName] = useState();
    const [token, setUserToken] = useState();
    const [password, setPassword] = useState();
    const [showErrorAlert, setShowErrorAlert] = useState(false)
    const handleSubmit = async e => {
        e.preventDefault();
        const userToken = await GetAuthToken({ username, password })
        console.log(userToken);

        if (userToken === 'error') {
            setShowErrorAlert(true)
            window.setTimeout(() => {
                setShowErrorAlert(false)
            }, 2000)
        } else {
            setUserToken(userToken);
            setToken(userToken);
        }
    }


    return ((!token) ? (

        <div className="maincontainer">
            <div class="container-fluid">
                <div class="row no-gutter">
                    <div class="col-md-6 d-none d-md-flex bg-image"></div>
                    <div className="col-md-6 bg-light">
                        <div className="login d-flex align-items-center py-5">
                            <div className="container">
                                <div className="row">
                                    <div className="col-lg-10 col-xl-7 mx-auto">
                                        <h2 className="display-4">Login page!</h2>
                                        <p className="text-muted mb-4">Parcel Management App.</p>
                                        <form onSubmit={handleSubmit}>
                                            <div className="mb-3">
                                                <input id="inputEmail" type="" placeholder="Username" required="" onChange={e => setUserName(e.target.value)} class="form-control rounded-pill border-0 shadow-sm px-4" />
                                            </div>
                                            <div className="mb-3">
                                                <input id="inputPassword" type="password" placeholder="Password" onChange={e => setPassword(e.target.value)} required="" class="form-control rounded-pill border-0 shadow-sm px-4 text-primary" />
                                            </div>
                                            <div className="mb-3">
                                                <Alert variant="danger" show={showErrorAlert} onClose={() => setShowErrorAlert(false)} dismissible>
                                                    <div className="d-flex justify-content-center">Incorrect Credentails!</div>
                                                </Alert>
                                            </div>
                                            <div className="form-check">
                                                <input id="customCheck1" type="checkbox" checked class="form-check-input" />
                                                <label for="customCheck1" class="form-check-label">Remember password</label>
                                            </div>
                                            <div className="d-grid gap-2 mt-2">
                                                <button type="submit" class="btn btn-primary btn-block text-uppercase mb-2 rounded-pill shadow-sm">Sign in</button>
                                            </div>

                                            <div className="text-center d-flex justify-content-between mt-4"><p>Code by <a class="font-italic text-muted">
                                                <u>KG</u></a></p></div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>
        // <div className="login-wrapper">
        //     <h1>Please Log In</h1>

        //     <Alert variant="error" show={showErrorAlert} onClose={() => setShowErrorAlert(false)} dismissible>
        //         <div className="d-flex justify-content-center">Incorrect Creds!</div>
        //     </Alert>
        //     <form onSubmit={handleSubmit}>
        //         <label>
        //             <p>Username</p>
        //             <input type="text" onChange={e => setUserName(e.target.value)} />
        //         </label>
        //         <label>
        //             <p>Password</p>
        //             <input type="password" onChange={e => setPassword(e.target.value)} />
        //         </label>
        //         <div>
        //             <button type="submit">Submit</button>
        //         </div>
        //     </form>
        // </div>
    ) : (
        <>
            <AppNav />
            <Router>
                <div>
                    <Switch>
                        <Route path="/parcel">
                            <ParcelApp token={token} />
                        </Route>
                        <Route path="/userApp">
                            <UserApp token={token} />
                        </Route>
                        <Route path="/ItemsApp">
                            <ItemsApp token={token} />
                        </Route>
                    </Switch>
                </div>
            </Router>
        </>
    )
    )
}

Login.propTypes = {
    setToken: PropTypes.func.isRequired
};