import React, { useState } from 'react'
import Button from 'react-bootstrap/Button'
import SaveIcon from '@material-ui/icons/Save';

export default function AddUserForm(props) {
    const initialUserFormState = { userKey: 0, userName: '', firstName: '', email: '', userPassword: '', addressKey: 0 }
    const initialAddressFormState = { addressKey: 0, cityName: '', address: '', addressRegion: '', addressPostalCode: 0 }
    const [user, setUser] = useState(initialUserFormState)
    const [address, setAddress] = useState(initialAddressFormState)

    const handleUserInputChange = event => {
        const { name, value } = event.target;
        setUser({ ...user, [name]: value })
    }

    const handleAddressInputChange = event => {
        const { name, value } = event.target;
        setAddress({ ...address, [name]: value })
    }
    return (
        <form
            onSubmit={(event) => {
                event.preventDefault()
                props.addUser(user)
                setAddress(initialAddressFormState)
                setUser(initialUserFormState)
            }}
        >
            <label>User Name</label>
            <input
                type="text"
                name="userName"
                value={user.userName}
                onChange={handleUserInputChange}
            />

            <label>First Name</label>
            <input
                type="text"
                name="firstName"
                value={user.firstName}
                onChange={handleUserInputChange}
            />

            <label>Email</label>
            <input
                type="text"
                name="email"
                value={user.email}
                onChange={handleUserInputChange}
            />

            <label>Password</label>
            <input
                type="password"
                name="userPassword"
                value={user.userPassword}
                onChange={handleUserInputChange}
            />

            <label>City Name</label>
            <input
                type="text"
                name="cityName"
                value={address.cityName}
                onChange={handleAddressInputChange}
            />

            <label>Address</label>
            <input
                type="text"
                name="address"
                value={address.address}
                onChange={handleAddressInputChange}
            />

            <label>Address Region</label>
            <input
                type="text"
                name="addressRegion"
                value={address.addressRegion}
                onChange={handleAddressInputChange}
            />

            <label>Address Postal Code</label>
            <input
                type="text"
                name="addressPostalCode"
                value={address.addressPostalCode}
                onChange={handleAddressInputChange}
            />
            <Button onClick={(event) => {
                props.addUser(user, address)
                event.preventDefault()
                setAddress(initialAddressFormState)
                setUser(initialUserFormState)
            }}><SaveIcon />Add New User</Button>
        </form>
    )
}