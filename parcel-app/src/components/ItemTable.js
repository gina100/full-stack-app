import React, { useState } from 'react'
import '../index.css'
import useSortableData from './sort.js'
import './styles.css';
import "bootstrap/dist/css/bootstrap.min.css";
import Table from "react-bootstrap/Table";
import Loader from 'react-loader-spinner';
import Alert from 'react-bootstrap/Alert'
import Button from 'react-bootstrap/Button'
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import Modal from 'react-bootstrap/Modal'
import CancelIcon from '@material-ui/icons/Cancel';
import ThumbUpIcon from '@material-ui/icons/ThumbUp';
import Pagination from 'react-bootstrap/Pagination'
import PageItem from 'react-bootstrap/PageItem'

const ItemTable = (props) => {
    const [showDeleteModal, setShowDeleteModal] = useState(false)
    const [showDeleteAlert, setShowDeleteAlert] = useState(false)
    const [item, setItem] = useState([])
    const deleteModal = (item) => {
        setItem(item)
        setShowDeleteModal(true)
    }

    const handleClose = () => setShowDeleteModal(false);
    const handleDelete = () => {
        setShowDeleteModal(false);
        props.deleteItem(item.itemKey)
        setShowDeleteAlert(true)
        window.setTimeout(() => {
            setShowDeleteAlert(false)
        }, 2000)
    }

    console.log(props.items)
    return ((

        <div>

            <>
                <Alert variant="success" show={showDeleteAlert} onClose={() => setShowDeleteAlert(false)} dismissible>
                    <div className="d-flex justify-content-center">The Item was deleted!</div>
                </Alert>
                <Modal size="sm" show={showDeleteModal} onHide={handleClose}>
                    <Modal.Header closeButton>
                        <Modal.Title>Are you sure you want to Delete the item</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>{item.itemName}</Modal.Body>
                    <Modal.Footer>
                        <Button variant="danger" onClick={handleClose}>
                            <CancelIcon />  Cancel
                        </Button>
                        <Button variant="success" onClick={handleDelete}>
                            <ThumbUpIcon />  Yes
                        </Button>
                    </Modal.Footer>
                </Modal>
            </>
            <Table striped bordered hover variant="dark">
                <thead>
                    <tr>
                        <th><Button variant="dark"
                        ><b>Item No.</b></Button></th>
                        <th>
                            <Button variant="dark"
                            ><b>Name</b></Button>
                        </th>
                        <th><Button variant="dark"
                        ><b>Deminsions</b></Button></th>
                        <th>Price</th>
                        <th>Description</th>
                        <th>Catergory</th>

                    </tr>
                </thead>{
                    props.items.length > 0 ? (
                        <tbody>
                            {
                                props.items.map((item) => (
                                    <tr key={item.itemKey}>
                                        <td>{item.itemNo}</td>
                                        <td>{item.itemName}</td>
                                        <td>{item.dimensions}</td>
                                        <td>{item.price}</td>
                                        <td>{item.description}</td>
                                        <td>{item.catergoryName}</td>
                                        <td>
                                            <EditIcon onClick={() => props.editItem(item)} />
                                            <DeleteIcon onClick={() => deleteModal(item)} />
                                        </td>
                                    </tr>
                                ))
                            }

                        </tbody>
                    ) : (
                        <div className="child-center">
                            <Loader type="ThreeDots" color="#2BAD60" height="60%" width="60%" />
                        </div>)}

            </Table>
            <Pagination>
                <Pagination.First />
                <Pagination.Prev />
                <Pagination.Item>{1}</Pagination.Item>
                <Pagination.Ellipsis />

                <Pagination.Item>{10}</Pagination.Item>
                <Pagination.Item>{11}</Pagination.Item>
                <Pagination.Item active>{12}</Pagination.Item>
                <Pagination.Item>{13}</Pagination.Item>
                <Pagination.Item disabled>{14}</Pagination.Item>

                <Pagination.Ellipsis />
                <Pagination.Item>{20}</Pagination.Item>
                <Pagination.Next />
                <Pagination.Last />
            </Pagination>
        </div >
    ))
}
export default ItemTable