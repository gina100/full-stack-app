import React, { useEffect, useState } from 'react'
import GetAllDescriptionies from './services/getAllDescriptionies.js'
import GetAllCatergories from './services/getAllCatergories'
import 'bootstrap/dist/css/bootstrap.min.css'
import Button from 'react-bootstrap/Button'
import SaveIcon from '@material-ui/icons/Save';

export default function EditItemForm(props) {
    const initialFormState = { itemKey: '', itemName: '', itemNo: '', deminsions: '', price: 0.0, descriptionKey: 0, description: '', catergoryKey: 0, catergoryName: '' }
    //const initialFormState = { parcelNumber: 0, parcelName: '', descriptionKey: 0, description: '', catergoryKey: 0, name: '', parcelWeight: 0, parcelWidth: 0, height: 0 }
    const [item, setItem] = useState(props.editedItem)
    const [descriptionies, setDescriptionies] = useState([])
    const [catergories, setCatergories] = useState([])
    const descriptionData = GetAllDescriptionies()
    const catergoryData = GetAllCatergories()

    useEffect(() => {
        setDescriptionies(descriptionData)
        setCatergories(catergoryData)
    }, [descriptionData, catergoryData])


    const handleInputChange = event => {
        const { name, value } = event.target;
        setItem({ ...item, [name]: value })
    }

    const handleCancel = event => {
        props.closeModal()
        props.setEditing(false)
    }
    return (
        <form
            onSubmit={(event) => {
                event.preventDefault()
                setItem(initialFormState)
            }}
        >
            <label>Name</label>
            <input
                type="text"
                name="itemName"
                value={item.itemName}
                onChange={handleInputChange}
            />

            <label>Item No.</label>
            <input
                type="text"
                name="itemNo"
                value={item.itemNo}
                onChange={handleInputChange}
            />
            <label>Dimensions</label>
            <input
                type="text"
                name="dimensions"
                value={item.dimensions}
                onChange={handleInputChange}
            />
            <label>Price</label>
            <input
                type="text"
                name="price"
                value={item.price}
                onChange={handleInputChange}
            />

            <label>
                Description:
                <select name="descriptionKey" value={item.descriptionKey} onChange={handleInputChange}>
                    <option value="0">Select description</option>
                    {
                        props.descriptionies.map((description) => (
                            <option value={description.descriptionKey}>{description.description}</option>
                        ))
                    }
                </select>
            </label>
            <label>
                Catergory :
                <select name="catergoryKey" value={item.catergoryKey} onChange={handleInputChange}>
                    <option value="0">Select catergory</option>
                    {
                        props.catergories.map((catergory) => (
                            <option value={catergory.catergoryKey}>{catergory.catergoryName}</option>
                        ))
                    }
                </select>
            </label>
            <Button onClick={(event) => {
                props.catergories
                    .filter(catergory => catergory.catergoryKey === item.catergoryKey)
                    .map(catergory =>
                        item.catergoryName = catergory.catergoryName,
                        setItem({ ...item })
                    )
                props.descriptionies
                    .filter(description => description.descriptionKey === item.descriptionKey)
                    .map((description) =>
                        item.description = description.description,
                        setItem({ ...item })
                    )
                props.updateItem(item)
                event.preventDefault()
                setItem(initialFormState)
            }}><SaveIcon />Update item</Button>
            <Button
                style={{
                    position: 'absolute',
                    right: 15
                }}
                variant="danger"
                onClick={handleCancel}>
                Cancel
            </Button>
        </form>
    )
}