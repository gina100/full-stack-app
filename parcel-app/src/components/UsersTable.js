import React, { useState } from 'react'
import '../index.css'
import useSortableData from './sort.js'
import './styles.css';
// import "bootstrap/dist/css/bootstrap.min.css";
import Table from "react-bootstrap/Table";
import IconButton from '@material-ui/core/IconButton';
import Button from 'react-bootstrap/Button'
import { makeStyles } from '@material-ui/core/styles';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import Box from '@material-ui/core/Box';
import Collapse from '@material-ui/core/Collapse';
import TableBody from '@material-ui/core/TableBody';
import Avatar from '@material-ui/core/Avatar';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import Alert from 'react-bootstrap/Alert'
import Modal from 'react-bootstrap/Modal'
import CancelIcon from '@material-ui/icons/Cancel';
import ThumbUpIcon from '@material-ui/icons/ThumbUp';

const UsersTable = (props) => {
    const [showDeleteModal, setShowDeleteModal] = useState(false)
    const [showDeleteAlert, setShowDeleteAlert] = useState(false)
    const [user, setUser] = useState([])
    const { items, requestSort, sortConfig } = useSortableData(props.users);

    const capitalize = (str) => {
        return str.split(' ').map(s => {
            return s.charAt(0).toUpperCase() + s.substr(1);
        }).join(' ');
    }

    const getClassNamesFor = (name) => {
        if (!sortConfig) {
            return;
        }
        return sortConfig.key === name ? sortConfig.direction : undefined;
    };
    const useRowStyles = makeStyles({
        root: {
            '& > *': {
                borderBottom: 'unset',
            },
        },
    });
    const deleteModal = (user) => {
        setUser(user)
        setShowDeleteModal(true)
    }
    const handleClose = () => setShowDeleteModal(false);
    const handleDelete = () => {
        setShowDeleteModal(false);
        props.deleteUser(user.userKey)
        setShowDeleteAlert(true)
        window.setTimeout(() => {
            setShowDeleteAlert(false)
        }, 2000)
    }

    const Row = (rows) => {
        const { row } = rows;
        const [open, setOpen] = useState(false);
        const classes = useRowStyles();

        return (
            <React.Fragment>
                <TableRow className={classes.root} onClick={() => setOpen(!open)} >
                    <td >
                        <IconButton aria-label="expand row" size="small" onClick={() => setOpen(!open)}>
                            {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
                        </IconButton>
                    </td>

                    <td align="right">{row.userName}</td>
                    <td align="right">{row.firstName}</td>
                    <td align="right">{row.email}</td>
                    <td>
                        <EditIcon onClick={() => props.editUser(row)} />
                        <DeleteIcon onClick={() => deleteModal(row)} />
                    </td>
                </TableRow>
                <TableRow>
                    <td style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
                        <Collapse in={open} timeout="auto" unmountOnExit>
                            <Box margin={1}>
                                <Table size="small" aria-label="purchases">
                                    <TableBody>                                    <td colSpan={6}>
                                        <div>
                                            <div>
                                                <Avatar>{capitalize(row.firstName).charAt(0)}</Avatar>
                                            </div>
                                            <div >
                                                <h3>{capitalize(row.firstName)}</h3>
                                                <p>
                                                    Address:<br />
                                                    <i>
                                                        {capitalize(row.address)}<br />
                                                        {capitalize(row.addressRegion)}<br />
                                                        {capitalize(row.cityName)}<br />
                                                        {row.addressPostalCode}
                                                    </i>
                                                </p>
                                            </div>
                                        </div>
                                    </td>
                                    </TableBody>
                                </Table>
                            </Box>
                        </Collapse>
                    </td>
                </TableRow>
            </React.Fragment>
        );
    }



    return (
        <div>
            <>
                <Alert variant="success" show={showDeleteAlert} onClose={() => setShowDeleteAlert(false)} dismissible>
                    <div className="d-flex justify-content-center">The User account was deleted!</div>
                </Alert>
                <Modal size="sm" show={showDeleteModal} onHide={handleClose}>
                    <Modal.Header closeButton>
                        <Modal.Title>Are you sure you want to Delete this user account</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>{user.userName}</Modal.Body>
                    <Modal.Footer>
                        <Button variant="danger" onClick={handleClose}>
                            <CancelIcon />  Cancel
                        </Button>
                        <Button variant="success" onClick={handleDelete}>
                            <ThumbUpIcon />  Yes
                        </Button>
                    </Modal.Footer>
                </Modal>
            </>
            <Table>
                <TableHead>
                    <TableRow>
                        <td />
                        <td align="right"><Button variant="dark" onClick={() => requestSort('userName')}
                            className={getClassNamesFor('userName')}>User Name</Button></td>
                        <td align="right">First Name</td>
                        <td align="right">Email</td>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {items.map((row) => (
                        <Row key={row.userKey} row={row} />
                    ))}
                </TableBody>
            </Table>
        </div>

    )
}
export default UsersTable


