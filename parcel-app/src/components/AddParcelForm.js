import React, { useState } from 'react'
import Button from 'react-bootstrap/Button'
import SaveIcon from '@material-ui/icons/Save';

export default function AddParcelForm(props) {
    const initialFormState = { parcelNumber: 0, parcelName: '', descriptionKey: 0, description: '', catergoryKey: 0, name: '', parcelWeight: 0, parcelWidth: 0, height: 0 }
    const [parcel, setParcel] = useState(initialFormState)

    const handleInputChange = event => {
        const { name, value } = event.target;
        setParcel({ ...parcel, [name]: value })
    }
    return (
        <form
            onSubmit={(event) => {
                console.log(parcel)
                event.preventDefault()
                props.addParcel(parcel)
                setParcel(initialFormState)
            }}
        >
            <label>Name</label>
            <input
                type="text"
                name="parcelName"
                value={parcel.parcelName}
                onChange={handleInputChange}
            />
            <label>
                Description:
                <select name="descriptionKey" value={parcel.descriptionKey} onChange={handleInputChange}>
                    <option value="0">Select description</option>
                    {
                        props.descriptionies.map((description) => (
                            <option value={description.descriptionKey}>{description.description}</option>
                        ))
                    }
                </select>
            </label>
            <label>
                Catergory :
                <select name="catergoryKey" value={parcel.catergoryKey} onChange={handleInputChange}>
                    <option value="0">Select catergory</option>
                    {
                        props.catergories.map((catergory) => (
                            <option value={catergory.catergoryKey}>{catergory.name}</option>
                        ))
                    }
                </select>
            </label>

            <label>Weight</label>
            <input
                type="number"
                name="parcelWeight"
                value={parcel.parcelWeight}
                onChange={handleInputChange}
            />
            <label>Width</label>
            <input
                type="number"
                name="parcelWidth"
                value={parcel.parcelWidth}
                onChange={handleInputChange}
            />
            <label>Height</label>
            <input
                type="number"
                name="height"
                value={parcel.height}
                onChange={handleInputChange}
            />


            <Button onClick={(event) => {
                props.catergories
                    .filter(catergory => parseInt(catergory.catergoryKey) === parseInt(parcel.catergoryKey))
                    .map(catergory =>
                        parcel.name = catergory.name,
                        setParcel({ ...parcel })
                    )
                props.descriptionies
                    .filter(description => parseInt(description.descriptionKey) === parseInt(parcel.descriptionKey))
                    .map((description) =>
                        parcel.description = description.description,
                        setParcel({ ...parcel })
                    )
                props.addParcel(parcel)
                event.preventDefault()
                setParcel(initialFormState)
            }}><SaveIcon />Add New Parcel</Button>
        </form>

    )
}
