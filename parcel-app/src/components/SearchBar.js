import React from 'react';

const SearchBar = (props) => {
    return (
        <div>
            <form className="form-inline my-2 my-lg-0">
                <input className="form-control mr-sm-2" type="search" placeholder="Search" value={props.input}
                    onChange={(e) => props.onChange(e.target.value)} aria-label="Search" />

            </form>
        </div>
    );
}
export default SearchBar