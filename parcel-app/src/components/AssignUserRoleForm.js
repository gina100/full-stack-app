import React, { useState } from 'react'
import Button from 'react-bootstrap/Button'
import SaveIcon from '@material-ui/icons/Save';

export default function AssignUserRolesForm(props) {
    const initialFormState = { userKey: 0, roleKey: 0 }
    const [userRole, setUserRole] = useState(initialFormState)
    const handleInputChange = event => {
        const { name, value } = event.target;
        setUserRole({ ...userRole, [name]: value })
    }
    return (
        <form
            onSubmit={(event) => {
                console.log(userRole)
                event.preventDefault()
                props.assignUserRole(userRole)
                setUserRole(initialFormState)
            }} >


            <label>
                User name:
                <select type="number" name="userKey" value={userRole.userKey} onChange={handleInputChange}>
                    <option value="0">Select user name</option>
                    {
                        props.users.map((user) => (
                            <option type="number" value={user.userKey}>{user.userName}</option>
                        ))
                    }
                </select>
            </label>
            <label>
                Roles :
                <select name="roleKey" value={userRole.roleKey} onChange={handleInputChange}>
                    <option value="0">Select role</option>
                    {
                        props.roles.map((role) => (
                            <option value={role.roleKey}>{role.roleName}</option>
                        ))
                    }
                </select>
            </label>
            <Button onClick={(event) => {
                props.assignUserRole(userRole)
                event.preventDefault()
                setUserRole(initialFormState)
            }}><SaveIcon />Assign Role</Button>
        </form>

    )
}
