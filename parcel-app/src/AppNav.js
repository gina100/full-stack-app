import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css'
import Nav from 'react-bootstrap/Nav'
import Navbar from 'react-bootstrap/Navbar'
import { BrowserRouter as Router } from "react-router-dom"

const AppNav = (props) => {
  return (
    <>
      <Navbar className="navbar-expand-lg" bg="dark" variant="dark">
        <Navbar.Brand href="/">
          <img alt="parcel Manager icon" src="/favicon.ico" width="30" height="30" className="d-inline-block align-top" />
          Parcel Manager
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav" className="justify-content-center">
          <Nav className="mr-auto">
            <Router>
              <li>
                <a className="nav-link" href="/parcel">Parcels<span className="sr-only">(current)</span></a>
              </li>
              <li>
                <a className="nav-link" href="/UserApp">User Account <span className="sr-only">(current)</span></a>
              </li>
              <li>
                <a className="nav-link" href="/ItemsApp">Items<span className="sr-only">(current)</span></a>
              </li>
            </Router>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    </>
  )
}

export default AppNav

