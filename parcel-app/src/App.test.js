import axios from 'axios';
import parcelUrl from './components/services/serverUrl';
import { fetchParcel } from './components/fetchParcel';
import parcels from './components/parcelData';
import { postParcel } from './components/postParcel';
import { deleteParcel } from './components/deleteParcel';
import { updateParcel } from './components/updateParcel';

jest.mock('axios');

describe('Parcel API tests', () => {
  it('fetches all parcels from the api', async () => {
    axios.get.mockImplementationOnce(() => Promise.resolve(parcels));
    await expect(fetchParcel('react')).resolves.toEqual(parcels);
    await expect(axios.get).toHaveBeenCalledWith(`${parcelUrl+'/parcel'}`);
  });

  it('post parcel api was called once', async () => {
    axios.post.mockImplementationOnce(() => Promise.resolve());
    await expect(axios.post).toHaveBeenCalledTimes(1);
  });

  it('delete parcel api was called once', async () => {
    axios.delete.mockImplementationOnce(() => Promise.resolve());
    await expect(axios.delete).toHaveBeenCalledTimes(1);
  });
  it('update parcel api was called once', async () => {
    axios.put.mockImplementationOnce(() => Promise.resolve());
    await expect(axios.put).toHaveBeenCalledTimes(1);
  });

});
