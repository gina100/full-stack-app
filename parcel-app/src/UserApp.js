import React, { useEffect, useState } from 'react';
import AddUsersForm from './components/AddUsersForm';
import AssignUserRoleForm from './components/AssignUserRoleForm';
import UpdateUser from './components/services/updateUser';
import PostUser from './components/services/addUser';
import PostUserRole from './components/services/addUserRole';
import GetUserRolesNumber from './components/services/getUserRolesNumber';
import PostAddress from './components/services/addAddress';
import GetAllAddresses from './components/services/getAllAddresses'
import GetAllUsers from './components/services/getAllUsers'
import GetAllRoles from './components/services/getAllRoles'
import EditUserForm from './components/EditUserForm';
import UsersTable from './components/UsersTable';
import UpdateAddress from './components/services/updateAddress';
import Alert from 'react-bootstrap/Alert'
import Modal from 'react-bootstrap/Modal'
import Button from 'react-bootstrap/Button'
import DeleteUser from './components/services/deleteUser';
function strCompare(str1, str2) {
  return str1 === str2;
}

const UserApp = (props) => {
  const usersData = GetAllUsers()
  const rolesData = GetAllRoles()
  const addresses = GetAllAddresses()
  const userRolesData = GetUserRolesNumber()
  const [users, setUsers] = useState([])
  const [roles, setRoles] = useState([])
  const [userRoles, setUserRoles] = useState([])
  const [userEditing, setUserEditing] = useState(false)
  const [currentUser, setCurrentUser] = useState([])
  const [currentAddress, setCurrentAddress] = useState([])
  const [modalStatus, setModalStatus] = useState(false)
  const [showAlert, setShowAlert] = useState(false)
  const [userRoleModalStatus, setUserRoleModalStatus] = useState(false)
  const [userStatus, setUserStatus] = useState("Create")
  const [message, setMessage] = useState("User was assigned!")
  const [variantValue, setVariantValue] = useState('success')
  const [rolesExist, setCheck] = useState(true)
  useEffect(() => {
    setUsers(usersData)
    setRoles(rolesData)
    setUserRoles(userRolesData)
  }, [usersData, rolesData, userRolesData])

  const addUser = (user, address) => {
    closeModal()
    const userAddress = {
      userKey: user.userKey, userName: user.userName, firstName: user.firstName, email: user.email, addressKey: user.addressKey, cityName: address.cityName, address: address.address, addressRegion: address.addressRegion, addressPostalCode: address.addressPostalCode
    }
    setUsers([...users, userAddress])
    PostAddress(address)
    console.log(addresses.length)
    user.addressKey = addresses[addresses.length - 1].addressKey
    console.log(user)
    PostUser(user)
  }
  const assignUserRole = (userRole) => {
    setVariantValue('success')
    const checkRoles = () => {
      const userKey = parseInt(userRole.userKey)
      const roleKey = parseInt(userRole.roleKey)

      for (const [i, userR] of userRoles.entries()) {
        if ((userR.userKey === userKey) && (userR.roleKey === roleKey)) {
          setMessage('User is already assigned to the selected role!')
          setVariantValue('danger')
          setShowAlert(true)

          window.setTimeout(() => {
            setShowAlert(false)
            setUserRoleModalStatus(false)
          }, 2000)
          return false
        }
      }
      return true
    }

    if (checkRoles() === true) {
      setMessage('User has been assigned to the selected role!')
      setShowAlert(true)
      window.setTimeout(() => {
        setShowAlert(false)
        setUserRoleModalStatus(false)
      }, 2000)
      PostUserRole(userRole)
      setUserRoles(...userRoles, userRole)
    }
  }
  const editUser = (user) => {
    setUserStatus('Update')
    openModal()
    setUserEditing(true)
    setCurrentAddress({ addressKey: user.addressKey, cityName: user.cityName, address: user.address, addressRegion: user.addressRegion, addressPostalCode: user.addressPostalCode })
    setCurrentUser({ userKey: user.userKey, addressKey: user.addressKey, userName: user.userName, firstName: user.firstName, email: user.email })
  }

  const updateUser = (updatedUser, updatedAddress) => {
    closeModal()
    setUserEditing(false)
    const userAddress = {
      userKey: updatedUser.userKey, userName: updatedUser.userName, firstName: updatedUser.firstName, email: updatedUser.email, addressKey: updatedAddress.addressKey, cityName: updatedAddress.cityName, address: updatedAddress.address, addressRegion: updatedAddress.addressRegion, addressPostalCode: updatedAddress.addressPostalCode
    }
    setUsers(users.map((user) => (user.userKey === updatedUser.userKey ? userAddress : user)))
    const recentUser = { userKey: updatedUser.userKey, userName: updatedUser.userName, firstName: updatedUser.firstName, email: updatedUser.email, addressKey: updatedAddress.addressKey }
    debugger;
    const recentAddress = { addressKey: updatedAddress.addressKey, cityName: updatedAddress.cityName, address: updatedAddress.address, addressRegion: updatedAddress.addressRegion, addressPostalCode: updatedAddress.addressPostalCode };
    console.log(recentAddress);
    UpdateAddress(recentAddress)
    UpdateUser(recentUser)
  }

  const openModal = () => {
    setModalStatus(true)
  }

  const openUserRoleModal = () => {
    setUserRoleModalStatus(true)
  }

  const deleteUser = (userKey) => {
    setUsers(users.filter((user) => user.userKey !== userKey))
    DeleteUser(userKey)
  }

  const closeUserRoleModal = () => (
    setUserRoleModalStatus(true)
  )
  const closeModal = () => {
    setModalStatus(false)
    setUserEditing(false)
  }

  return (
    <>
      <div >
        <Modal show={modalStatus} onHide={closeModal}>
          <Modal.Header closeButton>
            <Modal.Title><h4>{userStatus} An Account</h4></Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {userEditing ? (
              <div>
                <EditUserForm
                  closeModal={closeModal}
                  setUserEditing={setUserEditing}
                  currentUser={currentUser}
                  currentAddress={currentAddress}
                  updateUser={updateUser}
                />
              </div>
            ) : (
              <div>
                <AddUsersForm addUser={addUser} />
              </div>)}
          </Modal.Body>
        </Modal >
      </div>

      <div >
        <Modal show={userRoleModalStatus} onHide={closeUserRoleModal}>
          <Modal.Header closeButton>
            <Modal.Title><h4>{userStatus} Roles</h4></Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <div>
              <AssignUserRoleForm users={users} roles={roles} assignUserRole={assignUserRole} />
              <Alert variant={variantValue} show={showAlert} onClose={() => setShowAlert(false)} dismissible>
                <div className="d-flex justify-content-center">{message}</div>
              </Alert>
            </div>
          </Modal.Body>
        </Modal >
      </div>
      <div className="flex-large">
        <h2>View Users</h2> <Button onClick={openModal} size="sm">Create User Account</Button>
        <Button onClick={openUserRoleModal} style={{
          position: 'absolute',
          right: 0
        }} size="sm">User Roles</Button>
        <UsersTable users={users} updateUser={updateUser} deleteUser={deleteUser} editUser={editUser} />
      </div>
    </>
  )
}
export default UserApp