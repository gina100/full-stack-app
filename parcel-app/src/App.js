import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css'
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import AppNav from './AppNav';
import ParcelApp from './ParcelApp';
import ItemsApp from './ItemsApp';
import UserApp from './UserApp';
import Login from '../src/components/Login/Login';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

function setToken(userToken) {
  sessionStorage.setItem('token', JSON.stringify(userToken));
}
function getToken() {
  const tokenString = sessionStorage.getItem('token');
  const userToken = JSON.parse(tokenString);
  return userToken?.tokenString
}
const App = () => {
  const token = getToken();
  if (!token) {
    return <Login setToken={setToken} />
  }
  return (
    <>
      <AppNav />
      <Router>
        <div>
          <Switch>
            <Route path="/parcel">
              <ParcelApp token={token} />
            </Route>
            <Route path="/ItemsApp">
              <ItemsApp token={token} />
            </Route>
            <Route path="/userApp">
              <UserApp token={token} />
            </Route>
          </Switch>
        </div>
      </Router>
    </>)
}
export default App