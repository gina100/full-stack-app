import React, { useEffect, useState } from 'react';
import GetAllItems from './components/services/getAllItems';
import ItemTable from './components/ItemTable'
import EditItemForm from './components/EditItemForm'
import AddItemForm from './components/AddItemForm';
import PostItem from './components/services/addItem';
import DeleteItem from './components/services/deleteItem';
import UpdateParcel from './components/services/updateParcel';
import 'bootstrap/dist/css/bootstrap.min.css'
import Modal from 'react-bootstrap/Modal'
import { green } from '@material-ui/core/colors';
import Button from 'react-bootstrap/Button'
import AddIcon from '@material-ui/icons/Add';
import SearchBar from './components/SearchBar';

import GetAllDescriptionies from './components/services/getAllDescriptionies.js'
import GetAllCatergories from './components/services/getAllCatergories.js'
import UpdateItem from './components/services/updateItem';

const ItemsApp = (token) => {
  const initialFormState = { itemKey: '', itemName: '', itemNo: '', dimensions: '', price: 0.0, descriptionKey: 0, description: '', catergoryKey: 0, catergoryName: '' }

  const descriptionData = GetAllDescriptionies()
  const catergoryData = GetAllCatergories()
  //const [editing, setEditing] = useState(false)
  //const initialFormState = { parcelNumber: 0, parcelName: '', descriptionKey: 0, description: '', catergoryKey: 0, name: '', parcelWeight: 0, parcelWidth: 0, height: 0 }
  console.log(catergoryData);
  const itemData = GetAllItems(token)
  const [items, setItems] = useState([])
  const [descriptionies, setDescriptionies] = useState([])
  const [catergories, setCatergories] = useState([])
  const [modalStatus, setModalStatus] = useState(false)
  const [parcelStatus, setParcelStatus] = useState("Add")
  const [editing, setEditing] = useState(false)
  const [input, setInput] = useState('');
  const [editedItem, setEditedItem] = useState(initialFormState)
  useEffect(() => {
    setItems(itemData)
    setDescriptionies(descriptionData)
    setCatergories(catergoryData)
  }, [itemData])

  const addItem = (item) => {
    setItems([...items, item])
    PostItem(item)
    closeModal()
  }

  const openModal = () => {
    setModalStatus(true)
  }

  const closeModal = () => {
    setModalStatus(false)
    setEditing(false)
  }

  const updateInput = async (input) => {
    const filtered = itemData.filter(item => {
      return item.itemName.toLowerCase().includes(input.toLowerCase())
    })
    setInput(input);
    setItems(filtered);
  }
  const deleteItem = (id) => {
    setItems(items.filter((item) => item.itemKey !== id))
    DeleteItem(id)
  }

  const editItem = (item) => {
    setParcelStatus("Edit")
    openModal()
    setEditing(true)
    setEditedItem(item)
    // setCurrentParcel({ parcelNumber: parcel.parcelNumber, parcelName: parcel.parcelName, descriptionKey: parcel.descriptionKey, description: parcel.description, catergoryKey: parcel.catergoryKey, name: parcel.name, parcelWeight: parcel.parcelWeight, parcelWidth: parcel.parcelWidth, height: parcel.height })
  }
  const updateItem = (updatedItem) => {
    setEditing(false)
    setItems(items.map((item) => (item.itemKey === updatedItem.itemKey ? updatedItem : item)))
    const recentItem = { itemKey: updatedItem.itemKey, itemName: updatedItem.itemName, itemNo: updatedItem.itemNo, dimensions: updatedItem.dimensions, price: updatedItem.price, descriptionKey: updatedItem.descriptionKey, catergoryKey: updatedItem.catergoryKey }
    UpdateItem(recentItem)
    closeModal()
  }
  return (
    <>
      <div>
        <Modal show={modalStatus} onHide={closeModal}>
          <Modal.Header closeButton>
            <Modal.Title>{parcelStatus} Item Details</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {editing ? (
              <div>
                <EditItemForm
                  closeModal={closeModal}
                  setEditing={setEditing}
                  descriptionies={descriptionies}
                  catergories={catergories}
                  editedItem={editedItem}
                  updateItem={updateItem}
                />
              </div>
            ) : (
              <div>
                <AddItemForm descriptionies={descriptionies} catergories={catergories} addItem={addItem} token={token} />
              </div>
            )}
          </Modal.Body>
        </Modal >
      </div>
      <div className="flex-large">
        <h2>View Items</h2>
        <div className="d-flex">

          <div className="p-2"><Button onClick={openModal}><AddIcon className="fa fa-plus-circle" style={{ color: green[500] }} fontSize="small" />Add Items</Button></div>
          <div className="ml-auto p-2">
            <SearchBar input={input} onChange={updateInput} />
          </div>
        </div>
        <ItemTable deleteItem={deleteItem} editItem={editItem} items={items} />
      </div>
    </>
  )
}

export default ItemsApp


