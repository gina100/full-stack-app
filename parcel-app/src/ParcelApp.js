import React, { useEffect, useState } from 'react';
import GetAllParcels from './components/services/getAllParcels';
import ParcelTable from './components/ParcelTable'
import EditParcelForm from './components/EditParcelForm'
import AddParcelForm from './components/AddParcelForm';
import PostParcel from './components/services/addParcel';
import DeleteParcel from './components/services/deleteParcel';
import UpdateParcel from './components/services/updateParcel';
import 'bootstrap/dist/css/bootstrap.min.css'
import Modal from 'react-bootstrap/Modal'
import { green } from '@material-ui/core/colors';
import Button from 'react-bootstrap/Button'
import AddIcon from '@material-ui/icons/Add';
import SearchBar from './components/SearchBar';
import GetAllDescriptionies from './components/services/getAllDescriptionies.js'
import GetAllCatergories from './components/services/getAllCatergories.js'


const ParcelApp = (token) => {
  const descriptionData = GetAllDescriptionies()
  const catergoryData = GetAllCatergories()
  const [editing, setEditing] = useState(false)
  const initialFormState = { parcelNumber: 0, parcelName: '', descriptionKey: 0, description: '', catergoryKey: 0, name: '', parcelWeight: 0, parcelWidth: 0, height: 0 }
  const parcelData = GetAllParcels(token)
  const [parcels, setParcels] = useState([])
  const [currentParcel, setCurrentParcel] = useState(initialFormState)
  const [modalStatus, setModalStatus] = useState(false)
  const [parcelStatus, setParcelStatus] = useState("Add")
  const [input, setInput] = useState('');
  const [descriptionies, setDescriptionies] = useState([])
  const [catergories, setCatergories] = useState([])

  useEffect(() => {
    setParcels(parcelData)
    setDescriptionies(descriptionData)
    setCatergories(catergoryData)
  }, [parcelData, descriptionData, catergoryData])

  const addParcel = (parcel) => {
    setParcels([...parcels, parcel])
    PostParcel(parcel)
    closeModal()
  }

  const deleteParcel = (id) => {
    setParcels(parcels.filter((parcel) => parcel.parcelNumber !== id))
    DeleteParcel(id)
  }

  const editParcel = (parcel) => {
    setParcelStatus("Edit")
    openModal()
    setEditing(true)
    setCurrentParcel({ parcelNumber: parcel.parcelNumber, parcelName: parcel.parcelName, descriptionKey: parcel.descriptionKey, description: parcel.description, catergoryKey: parcel.catergoryKey, name: parcel.name, parcelWeight: parcel.parcelWeight, parcelWidth: parcel.parcelWidth, height: parcel.height })
  }

  const updateParcel = (updatedParcel) => {
    setEditing(false)
    setParcels(parcels.map((parcel) => (parcel.parcelNumber === updatedParcel.parcelNumber ? updatedParcel : parcel)))
    const recentParcel = { parcelNumber: updatedParcel.parcelNumber, parcelName: updatedParcel.parcelName, descriptionKey: updatedParcel.descriptionKey, catergoryKey: updatedParcel.catergoryKey, parcelWeight: updatedParcel.parcelWeight, parcelWidth: updatedParcel.parcelWidth, height: updatedParcel.height }
    UpdateParcel(recentParcel)
    closeModal()
  }

  const openModal = () => {
    setModalStatus(true)
  }

  const closeModal = () => {
    setModalStatus(false)
    setEditing(false)
  }

  const updateInput = async (input) => {
    const filtered = parcelData.filter(parcel => {
      return parcel.parcelName.toLowerCase().includes(input.toLowerCase())
    })
    setInput(input);
    setParcels(filtered);
  }

  return (
    <>
      <div>
        <Modal show={modalStatus} onHide={closeModal}>
          <Modal.Header closeButton>
            <Modal.Title>{parcelStatus} Parcel Details</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {editing ? (
              <div>
                <EditParcelForm
                  closeModal={closeModal}
                  setEditing={setEditing}
                  currentParcel={currentParcel}
                  updateParcel={updateParcel}
                />
              </div>
            ) : (
              <div>
                <AddParcelForm descriptionies={descriptionies} catergories={catergories} addParcel={addParcel} token={token} />
              </div>
            )}
          </Modal.Body>
        </Modal >
      </div>

      <div className="flex-large">
        <h2>View parcels</h2>
        <div className="d-flex">

          <div className="p-2"><Button onClick={openModal}><AddIcon className="fa fa-plus-circle" style={{ color: green[500] }} fontSize="small" />Add Parcel</Button></div>
          <div className="ml-auto p-2">
            <SearchBar input={input} onChange={updateInput} />
          </div>
        </div>
        <ParcelTable parcels={parcels} editParcel={editParcel} deleteParcel={deleteParcel} />
      </div>
    </>
  )
}

export default ParcelApp


