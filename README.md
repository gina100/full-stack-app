# Parcel Management App

Simple parcel management app with React.js Asp.net core and Postgres

## Table of contents

*[General info](#general-info)

*[Technologies](#technologies)

*[Setup](#setup)

## General info

A parcel delivery app that offers a convenient way to track your packages in real time, from the moment they are registered in the system up to the moment they  reach their destination and delivered to the address.

##  Technologies
Project is created with:
* React
* Postgres sql
* Dapper Version 2.0.78
* ASP.NET Core 


## Setup
To run this project 
* First download and install [Docker](https://www.docker.com/get-started)
Create a Docker file to build a custom image and container that will be used to host the postgres database.
* Dapper will used for mapping between database and C# .
    Add Dapper Nuget Package by  pasting the following command :
````
pm> Install-Package Dapper -Version 2.0.78
````

* React UI,
    Install React by npm install and start the project by npm start







 