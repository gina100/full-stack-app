Create procedure insert_roles(rolekey varchar,rolename varchar)
language sql  
AS $$
INSERT INTO public.roles(rolekey, rolename)VALUES (rolekey, rolename);
$$;
Create procedure select_roles()
language sql  
AS $$
select * from public.roles;
$$;