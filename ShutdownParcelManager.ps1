Write-Host "Shutdown"

$proc1 = (start-process -PassThru .\StopParcelApp.bat)

Taskkill.exe /F /IM dotnet.exe /T
docker stop parcel-container
Taskkill /F /IM "docker desktop.exe" /T