Create procedure get_all_items()
language sql  
AS $$
SELECT * FROM public.item INNER JOIN public.itemDescription ON Item.DescriptionKey=itemDescription.DescriptionKey INNER JOIN public.itemCatergory ON Item.catergoryKey=itemCatergory.CatergoryKey
$$;