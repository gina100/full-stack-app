create table parcel(
		ParcelNumber SERIAL PRIMARY KEY,
		DescriptionKey int,
		CatergoryKey int,
		ParcelName varchar(255)not null,
		ParcelWeight decimal, 
		ParcelWidth decimal,
		Height decimal,
		FOREIGN KEY (DescriptionKey) REFERENCES parcelDescription(DescriptionKey),
		FOREIGN KEY (CatergoryKey) REFERENCES parcelCatergory(CatergoryKey)
);

create table parcelCatergory(
		CatergoryKey SERIAL PRIMARY KEY,
		Name varchar(255)not null
);

create table parcelDescription(
		DescriptionKey SERIAL PRIMARY KEY,
		Description varchar(255)not null
);

create table Addresses(
		AddressKey SERIAL PRIMARY KEY,
		CityName varchar(255)not null,
		Address varchar(50) not null,
		AddressRegion varchar(50) not null,
		AddressPostalCode varChar(50)
);

create table Users(
		UserKey SERIAL PRIMARY KEY,
		UserName varchar(255)not null UNIQUE,
		UserPassword varchar(255)not null,
		FirstName varchar(50) not null,
		Email varchar(50) not null Unique,
		AddressKey int,
		FOREIGN KEY (AddressKey) REFERENCES Addresses(AddressKey)
);

create table Roles(
		RoleKey SERIAL PRIMARY key,
		RoleName varchar(50) not null Unique
);

create table UserRole(
		UserKey int,
		RoleKey int,
		primary key (UserKey,RoleKey),
		FOREIGN KEY (UserKey) REFERENCES Users(UserKey),
		FOREIGN KEY (RoleKey) REFERENCES Roles(RoleKey)
);