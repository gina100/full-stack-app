
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Microsoft.Extensions.Logging;
using ParcelManagement.Data;
using ParcelManagement.Interfaces;
using ParcelManagement.BusinessLogic;

namespace ParcelManagement
{
    public class Startup
    {
        readonly string MyAllowSpecificOrigins = "AllowSpecificOrigin";
       
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            connectionString = configuration.GetValue<string>("DBInfo:ParcelConnection");
        }

        public IConfiguration Configuration { get; }
        private string connectionString; 
        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddJwtBearer(options =>
            {
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,
                    ValidIssuer = Configuration["Jwt:Issuer"],
                    ValidAudience = Configuration["Jwt:Issuer"],
                    IssuerSigningKey =
                        new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["Jwt:Key"])),
                    RequireExpirationTime = true
                };
            });
            services.AddCors(options =>
            {
                options.AddPolicy(name: MyAllowSpecificOrigins, builder =>
                {
                    builder.AllowAnyOrigin().AllowAnyHeader().AllowAnyMethod();
                });
            });
            services.Add(new ServiceDescriptor(typeof(IParcelRepository), new ParcelRepository(connectionString)));
            services.Add(new ServiceDescriptor(typeof(IParcelItemRepository), new ParcelItemRepository(connectionString)));
            services.Add(new ServiceDescriptor(typeof(IItemRepository), new ItemRepository(connectionString)));
            services.Add(new ServiceDescriptor(typeof(IItemDescriptionRepository), new ItemDescriptionRepository(connectionString)));
            services.Add(new ServiceDescriptor(typeof(IItemCatergoryRepository), new ItemCatergoryRepository(connectionString)));
            services.Add(new ServiceDescriptor(typeof(IAddressesRepository), new AddressesRepository(connectionString)));
            services.Add(new ServiceDescriptor(typeof(IUsersRepository), new UsersRepository(connectionString)));
            services.Add(new ServiceDescriptor(typeof(IRolesRepository), new RolesRepository(connectionString)));
            services.Add(new ServiceDescriptor(typeof(IAccountBusiness), new AccountBusiness(Configuration,new UsersRepository(connectionString),new RolesRepository(connectionString))));

            services.AddSingleton<IConfiguration>(Configuration);
            services.AddControllers();
            services.AddControllers().AddNewtonsoftJson();
            services.AddSwaggerGen();
            //services.AddSwaggerGen(c =>
            //{
            //    c.SwaggerDoc("v1", new OpenApiInfo
            //    {
            //        Version = "v1",
            //        Title = "Parcel Management",
            //        Description = "API documentation for parcel management app"
            //    });
            //    var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
            //    var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
            //    c.IncludeXmlComments(xmlPath);
            //});

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddFile("Logs/mylog-{Date}.txt");

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseStaticFiles();
            
            app.UseHttpsRedirection();
            //app.UseStaticFiles();
            app.UseRouting();
            app.UseCors(builder => builder.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Parcel Management API V1");
                c.InjectStylesheet("/swagger-ui/custom.css");
            });
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }    
    }
}

