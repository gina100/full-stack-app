﻿using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Mvc;
using ParcelManagement.Models;
using ParcelManagement.Data;
using ParcelManagement.Interfaces;
using Microsoft.AspNetCore.Http;

namespace ParcelManagement.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : Controller
    {
        IUsersRepository _usersRepository;
        IRolesRepository _rolesRepository;

        public UsersController(IUsersRepository usersRepository,IRolesRepository rolesRepository)
        {
            _usersRepository = usersRepository;
            _rolesRepository = rolesRepository;
        }
        /// <summary>
        /// Returns all users.
        /// </summary>
        [HttpGet]
        public ActionResult GetAllUsers()
        {
            var data = _usersRepository.GetAllUsers();

            return Ok(data);
        }

		[HttpGet]
        [Route("GetAllRoles")]
        public ActionResult GetAllRoles()
        {
            var data = _rolesRepository.GetAllRolez();

            return Ok(data);
        }

        [HttpGet]
        [Route("GetUserRolesNumber")]
        public ActionResult GetUserRolesNumber()
        {
            var data = _rolesRepository.GetUserRoles();

            return Ok(data);
        }

        [HttpPost()]
        [Route("AddRoles")]
        public ActionResult AddRoles(Roles roles)
        {
            _rolesRepository.AddRoles(roles);

            return Ok();
        }

        [HttpPost()]
        [Route("AddUserRoles")]
        public ActionResult AddUserRoles(UserRoles userRoles)
        {
            _rolesRepository.AddUserRoles(userRoles);

            return Ok();
        }
		
		/// <summary>
        /// Returns a specific user.
        /// </summary>
        /// <param name="id"></param>
        [HttpGet]
        [Route("GetUserById")]
        public ActionResult GetUserById(string id)
        {
            var data =  _usersRepository.GetUserByKey(id);
            if (data == null) return NotFound();

            return Ok(data);
        }
        /// <summary>
        /// Creates a new user.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /User
        ///     {
        ///        "UserKey": 1,
        ///        "UserName": "Kgina",
        ///        "Firstname": "Kwenza",
        ///        "Email": "suphiwok@gmail.com",
        ///        "AddressKey":4
        ///     }
        ///
        /// </remarks>
        /// <param name="user"></param>
        /// <returns>the created user</returns>
        /// <response code="201">Returns the created user</response>
        /// <response code="400">If the user is null</response>    
        [HttpPost()]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult AddUser(Users user)
        {
            user.UserPassword = BCrypt.Net.BCrypt.HashPassword(user.UserPassword);
            user.LockOut = false;
            _usersRepository.AddUser(user);

            return Ok(user);
        }

        /// <summary>
        /// Deletes a specific user.
        /// </summary>
        /// <param name="userKey"></param>
        [HttpDelete()]
        public ActionResult DeleteUserById(string userKey)
        {
            _usersRepository.DeleteUserByKey(userKey);
           
            return Ok();
        }

        /// <summary>
        /// Updates an existing user.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /User
        ///     {
        ///        "UserKey": 1,
        ///        "UserName": "Kgina",
        ///        "Firstname": "Kwenza",
        ///        "Email": "suphiwot@gmail.com",
        ///        "AddressKey":4
        ///     }
        ///
        /// </remarks>
        /// <param name="user"></param>
        /// <returns>The updated user</returns>
        [HttpPut]
        public ActionResult UpdateUser(Users user)
        {
            _usersRepository.UpdateUser(user);

            return Ok(user);
        }
    }
}
