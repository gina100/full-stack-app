﻿using Microsoft.AspNetCore.Mvc;
using ParcelManagement.Interfaces;

namespace ParcelManagement.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class CatergoryController  : Controller
    {
        IItemCatergoryRepository _catergoryRepository;

        public CatergoryController(IItemCatergoryRepository catergoryRepository)
        {           
            _catergoryRepository = catergoryRepository;
        }

        /// <summary>
        /// Returns all Catergories.
        /// </summary>
        [HttpGet]
        public ActionResult GetAllCatergories()
        {
            var data = _catergoryRepository.GetAllItemCatergories();

            return Ok(data);
        }
    }
}
