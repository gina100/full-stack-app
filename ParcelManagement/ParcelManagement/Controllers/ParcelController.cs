﻿using Microsoft.AspNetCore.Mvc;
using ParcelManagement.Models;
using ParcelManagement.Interfaces;

using Microsoft.VisualStudio.Services.DelegatedAuthorization;


namespace ParcelManagement.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class ParcelController : Controller
    {
        IParcelRepository _parcelRepository;

        public ParcelController(IParcelRepository parcelRepository)
        {
            _parcelRepository = parcelRepository;
        }

        /// <summary>
        /// Returns all Parcels.
        /// </summary>
        [Authorization("user")]
        [HttpGet]
        public ActionResult GetAllParcels()
        {
            var data = _parcelRepository.GetAllParcels();

            return Ok(data);
        }

        /// <summary>
        /// Returns all address.
        /// </summary>
        /// <param name="parcelKey"></param>
        [HttpGet()]
        [Route("GetParcelByParcelKey")]
        public ActionResult GetByParcelNumber(string parcelKey)
        {
            var data =  _parcelRepository.GetParcelByParcelKey(parcelKey);
            if (data == null) return NotFound();

            return Ok(data);
        }

        /// <summary>
        /// Creates a parcel.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /Parcel
        ///     {
        ///        "ParcelNumber": 1,
        ///        "ParcelName": "Head Phones",
        ///        "AddressKey": 2,
        ///        "CatergoryKey": 6,
        ///        "UserKey":100,
        ///        "ParcelWeight":1.2,
        ///        "ParcelWidth":3.4,
        ///        "Height":2.3
        ///     }
        ///
        /// </remarks>
        /// <param name="parcel"></param>
        /// <returns>The created parcel</returns>
        /// <response code="201">Returns the created parcel</response>
        /// <response code="400">If the parcel is null</response> 
        [HttpPost()]
        public ActionResult AddParcel(Parcel parcel)
        {
            _parcelRepository.AddParcel(parcel);

            return Ok();
        }

        /// <summary>
        /// Deletes a specific parcel.
        /// </summary>
        /// <param name="parcelKey"></param>
        [HttpDelete()]
        public ActionResult DeleteParcelByParcelKey(string parcelKey)
        {
            _parcelRepository.DeleteParcelByParcelKey(parcelKey);
           
            return Ok();
        }

        /// <summary>
        /// Updates an existing parcel.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /Parcel
        ///     {
        ///        "ParcelNumber": 1,
        ///        "ParcelName": "Head Phones",
        ///        "AddressKey": 2,
        ///        "CatergoryKey": 6,
        ///        "UserKey":100,
        ///        "ParcelWeight":1.2,
        ///        "ParcelWidth":3.4,
        ///        "Height":2.3
        ///     }
        ///
        /// </remarks>
        /// <param name="parcel"></param>
        /// <returns>The updated parcel</returns>
        [HttpPut]
        public ActionResult UpdateParcel(Parcel parcel)
        {
            _parcelRepository.UpdateParcel(parcel);

            return Ok(parcel);
        }
    }
}
