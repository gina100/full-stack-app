﻿using Microsoft.AspNetCore.Mvc;
using ParcelManagement.Models;
using ParcelManagement.Interfaces;

using Microsoft.VisualStudio.Services.DelegatedAuthorization;


namespace ParcelManagement.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class ItemController : Controller
    {
        IItemRepository _itemRepository;

        public ItemController(IItemRepository itemRepository)
        {
            _itemRepository = itemRepository;
        }

        /// <summary>
        /// Returns all Parcels.
        /// </summary>
       // [Authorization("user")]
        [HttpGet]
        public ActionResult GetAllItems()
        {
            var data = _itemRepository.GetAllItems();

            return Ok(data);
        }

        /// <summary>
        /// Returns all address.
        /// </summary>
        /// <param name="itemKey"></param>
        [HttpGet()]
        [Route("GetItemByItemKey")]
        public ActionResult GetByItemKey(string itemKey)
        {
            var data = _itemRepository.GetItemByItemKey(itemKey);
            if (data == null) return NotFound();

            return Ok(data);
        }

        /// <summary>
        /// Creates a parcel.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /Parcel
        ///     {
        ///        "ParcelNumber": 1,
        ///        "ParcelName": "Head Phones",
        ///        "AddressKey": 2,
        ///        "CatergoryKey": 6,
        ///        "UserKey":100,
        ///        "ParcelWeight":1.2,
        ///        "ParcelWidth":3.4,
        ///        "Height":2.3
        ///     }
        ///
        /// </remarks>
        /// <param name="item"></param>
        /// <returns>The created parcel</returns>
        /// <response code="201">Returns the created parcel</response>
        /// <response code="400">If the parcel is null</response> 
        [HttpPost()]
        public ActionResult AddItem(Item item)
        {
            _itemRepository.AddItem(item);

            return Ok(item);
        }

        /// <summary>
        /// Deletes a specific parcel.
        /// </summary>
        /// <param name="itemKey"></param>
        [HttpDelete()]
        public ActionResult DeleteItemByItemKey(string itemKey)
        {
            _itemRepository.DeleteItemByItemKey(itemKey);

            return Ok();
        }

        /// <summary>
        /// Updates an existing parcel.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /Parcel
        ///     {
        ///        "ParcelNumber": 1,
        ///        "ParcelName": "Head Phones",
        ///        "AddressKey": 2,
        ///        "CatergoryKey": 6,
        ///        "UserKey":100,
        ///        "ParcelWeight":1.2,
        ///        "ParcelWidth":3.4,
        ///        "Height":2.3
        ///     }
        ///
        /// </remarks>
        /// <param name="item"></param>
        /// <returns>The updated parcel</returns>
        [HttpPut]
        public ActionResult UpdateItem(Item item)
        {
            _itemRepository.UpdateItem(item);

            return Ok(item);
        }
    }
}
