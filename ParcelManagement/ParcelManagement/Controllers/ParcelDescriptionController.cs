﻿using Microsoft.AspNetCore.Mvc;
using ParcelManagement.Interfaces;

namespace ParcelManagement.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class ParcelDescriptionController  : Controller
    {
        IItemDescriptionRepository _descriptionRepository;

        public ParcelDescriptionController(IItemDescriptionRepository descriptionRepository)
        {
            _descriptionRepository = descriptionRepository;
        }

        /// <summary>
        /// Returns all parcel descriptionies.
        /// </summary>
        [HttpGet]
        public ActionResult GetAllItemDescriptions()
        {
            var data = _descriptionRepository.GetAllItemDescriptions();

            return Ok(data);
        }
    }
}
