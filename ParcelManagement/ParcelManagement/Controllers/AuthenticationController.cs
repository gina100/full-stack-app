﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using ParcelManagement.Models;
using ParcelManagement.Interfaces;
using Microsoft.Extensions.Logging;

namespace ParcelManagement.Controllers
{
    [Produces("application/json")]
    [Route("api/auth")]
    [ApiController]
    public class AuthenticationController : ControllerBase
    {
        IAccountBusiness _accountBusiness;
        //private ILogger _logger;

        public AuthenticationController( ILoggerFactory logFactory,IAccountBusiness accountBusiness)
        {
           // _logger = logFactory.CreateLogger<AuthenticationController>();
            _accountBusiness = accountBusiness;
        }

        /// <summary>
        /// Get token, authenticating user credentials
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        [Route("token")]
        [AllowAnonymous]
        [HttpPost]
        public IActionResult CreateToken([FromBody] TokenRequest login)
        {
           // _logger.LogInformation("Log message in CreateToken method");
            IActionResult response = Unauthorized();
            var userObj = _accountBusiness.AuthenticateUser(login);
            
            
            if (userObj.Email != null)
            {
                var tokenObj = _accountBusiness.GenerateToken(userObj);
                if (tokenObj != new object())
                {
                    response = Ok(tokenObj);
                }
            }

            return response;
        }
    }
}
