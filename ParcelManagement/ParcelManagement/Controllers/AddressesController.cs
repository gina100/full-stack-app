﻿using Microsoft.AspNetCore.Mvc;
using ParcelManagement.Models;
using ParcelManagement.Interfaces;
using Microsoft.AspNetCore.Http;

namespace ParcelManagement.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class AddressesController : Controller
    {
         IAddressesRepository _addressesRepository;

        public AddressesController(IAddressesRepository addressesRepository)
        {
            _addressesRepository = addressesRepository;
        }
        /// <summary>
        /// Returns all address.
        /// </summary>
        [HttpGet]
        public ActionResult GetAllAddresses()
        {
            var data = _addressesRepository.GetAllAddresses();

            return Ok(data);
        }
        /// <summary>
        /// Returns a specific address.
        /// </summary>
        /// <param name="addressKey"></param>
        [HttpGet()]
        [Route("GetAddressByKey")]
        public ActionResult GetAddressById(string addressKey)
        {
            var data =  _addressesRepository.GetAddressByKey(addressKey);
            if (data == null) return NotFound();

            return Ok(data);
        }
        /// <summary>
        /// Creates an address.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /Address
        ///     {
        ///        "AddressKey": 1,
        ///        "CityName": "Durban",
        ///        "Address": "New Germany",
        ///        "AddressRegion": "RethMan road 34",
        ///        "AddressPostalcode":3645
        ///     }
        ///
        /// </remarks>
        /// <param name="address"></param>
        /// <returns>A newly created address</returns>
        /// <response code="201">Returns the created address</response>
        /// <response code="400">If the address is null</response>    
        [HttpPost()]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult AddAddress(Addresses address)
        {
            _addressesRepository.AddAddress(address);

            return Ok(address);
        }
        /// <summary>
        /// Deletes a specific address.
        /// </summary>
        /// <param name="addressKey"></param>
        [HttpDelete()]
        public ActionResult DeleteAddressByKey(string addressKey)
        {
            _addressesRepository.DeleteAddressByKey(addressKey);
           
            return Ok();
        }
        /// <summary>
        /// Updates an existing address record.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /Address
        ///     {
        ///        "AddressKey": 1,
        ///        "CityName": "Durban",
        ///        "Address": "New Germany",
        ///        "AddressRegion": "RethMan road 34",
        ///        "AddressPostalcode":3645
        ///     }
        ///
        /// </remarks>
        /// /// <returns>An updated address</returns>
        [HttpPut]
        public ActionResult UpdateAddress(Addresses address)
        {
            _addressesRepository.UpdateAddress(address);

            return Ok(address);
        }
    }
}
