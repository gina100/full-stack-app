﻿
namespace ParcelManagement.Models
{
    public class Users
    {
        public string UserKey { get; set; }
        public string UserName { get; set; }
        public string UserPassword { get; set; }
        public string FirstName  { get; set; }
        public string Email { get; set; }
        public string AddressKey { get; set; }
        public bool LockOut { get; set; }
    }
}
