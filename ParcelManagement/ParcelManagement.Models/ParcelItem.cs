﻿namespace ParcelManagement.Models
{
    public class ParcelItem
    {
        public string ParcelKey { get; set; }
        public string ItemKey { get; set; }
        public int Quantity { get; set; }
    }
}
