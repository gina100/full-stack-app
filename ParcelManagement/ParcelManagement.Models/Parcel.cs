﻿using System;

namespace ParcelManagement.Models
{
    public class Parcel
    {
        public Parcel GetParcelData(string parcelKey, string parcelNumber, string userKey, string preferedDropOffAddress, DateTime dateCreated)
        {
            ParcelKey = parcelKey;
            ParcelNumber = parcelNumber;
            UserKey = userKey;
            PreferedDropOffAddress = preferedDropOffAddress;
            DateCreated = dateCreated;

            return this;
        }

        public string ParcelKey { get; set; }
        public string ParcelNumber { get; set; }
        public string UserKey { get; set; }
        public string PreferedDropOffAddress { get; set; }
        public DateTime DateCreated { get; set; }
    }
}
