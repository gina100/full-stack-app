﻿namespace ParcelManagement.Models
{
    public class Item
    {
        public string ItemKey { get; set; }
        public string ItemName { get; set; }
        public string ItemNo { get; set; }
        public string CatergoryName { get; set; }
        public string Description { get; set; }
        public string DescriptionKey { get; set; }
        public string CatergoryKey { get; set; }
        public string Dimensions { get; set; }
        public decimal Price { get; set; }
    }
}
