﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ParcelManagement.Models
{
   public class TokenDetails
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Roles { get; set; }
    }
}
