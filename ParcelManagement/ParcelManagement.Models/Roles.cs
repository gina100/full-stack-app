﻿
namespace ParcelManagement.Models
{
    public class Roles
    {
        public string RoleKey { get; set; }
        public string RoleName { get; set; }
    }
}
