﻿
namespace ParcelManagement.Models
{
    public class ItemDescription
    {
        public string DescriptionKey { get; set; }
        public string Description { get; set; }
    }
}
