﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ParcelManagement.Models
{
    public class UserAddresses
    {
        public string AddressKey { get; set; }
        public string UserKey { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string Email { get; set; }
        public string CityName { get; set; }
        public string Address { get; set; }
        public string AddressRegion { get; set; }
        public string AddressPostalCode { get; set; }
    }
}
