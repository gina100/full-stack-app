﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ParcelManagement.Models
{
    public class TokenRequest
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
