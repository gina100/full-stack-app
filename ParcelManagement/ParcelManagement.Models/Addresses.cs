﻿namespace ParcelManagement.Models
{
    public class Addresses
    {
        public string AddressKey { get; set; }
        public string CityName { get; set; }
        public string Address { get; set; }
        public string AddressRegion { get; set; }
        public string AddressPostalCode { get; set; }
    }
}
