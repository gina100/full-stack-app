﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using ParcelManagement.Interfaces;
using ParcelManagement.Models;

namespace ParcelManagement.BusinessLogic
{
    public class AccountBusiness : IAccountBusiness
    {
        IConfiguration _configuration;
        IUsersRepository _usersRepository;
        IRolesRepository _rolesRepository;
        public AccountBusiness(IConfiguration configuration,IUsersRepository usersRepository, IRolesRepository rolesRepository)
        {
            _configuration = configuration;
            _rolesRepository = rolesRepository;
            _usersRepository = usersRepository;
        }

        public TokenDetails AuthenticateUser(TokenRequest tokenRequest)
        {
            var tokenDetails = new TokenDetails();
            var user = _usersRepository.GetUserByCredentails(tokenRequest.Username);
            if (user != null)
            {
                bool verified = BCrypt.Net.BCrypt.Verify(tokenRequest.Password, user.UserPassword);

                if (verified)
                {
                    tokenDetails.Email = user.Email;
                    tokenDetails.Name = user.FirstName;

                    var userRoles = _rolesRepository.GetAllUserRolesByUserKey(user.UserKey);

                    foreach (var userRole in userRoles)
                    {
                        tokenDetails.Roles = _rolesRepository.GetRolesByRoleKey(userRole.RoleKey).RoleName;
                    }
                }
            }
            return tokenDetails;
        }

        public object GenerateToken(TokenDetails user)
        {
            var tokenString = new object();
            
            if (user.Name != null)
            {
                var claims = new Claim[]
                {
                    new Claim(ClaimTypes.Name, user.Name),
                    new Claim("Roles",user.Roles)
                };
               tokenString = buildToken(claims);
            }

            return tokenString;
        }

        private object buildToken(Claim[] claims)
        {
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Jwt:Key"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var expiryTime = DateTime.Now.AddMinutes(Convert.ToDouble(_configuration["Jwt:ExpiryMinutes"]));
            var token = new JwtSecurityToken(_configuration["Jwt:Issuer"],
                _configuration["Jwt:Issuer"],
                expires: expiryTime,
                claims: claims,
                signingCredentials: creds);
            var details = new
            {
                tokenString = new JwtSecurityTokenHandler().WriteToken(token),
                expiryDate = expiryTime.ToString("MM/dd/yyyy HH:mm:ss")
            };
            return details;
        }
    }
}
