﻿using ParcelManagement.Models;

namespace ParcelManagement.Tests
{
    public class ParcelBuilder
    {
        private int _parcelNumber = 1;
        private string _parcelName = "Samsung S10";
        private int _descriptionId = 1;
        private int _catergoryId = 1;
        private decimal _parcelWeight = 0.55M;
        private decimal _parcelWidth = 0.3M;
        private decimal _height = 1.4M;

        public ParcelBuilder WithParcelNumber(int parcelNumber)
        {
            _parcelNumber = parcelNumber;
            return this;
        }

        public ParcelBuilder WithParcelName(string parcelName)
        {
            _parcelName = parcelName;
            return this;
        }
        
        public ParcelBuilder WithParcelDescriptionId(int parcelDescription)
        {
            _descriptionId = parcelDescription;
            return this;
        }
        
        public ParcelBuilder WithCatergoryId(int catergory)
        {
            _catergoryId = catergory;
            return this;
        }
        
        public ParcelBuilder WithParcelWeight(decimal parcelWeight)
        {
            _parcelWeight =parcelWeight;
            return this;
        }
        
        public ParcelBuilder WithParcelWidth(decimal parcelWidth)
        {
            _parcelWidth = parcelWidth;
            return this;
        }

        public ParcelBuilder WithHeight(decimal height)
        {
            _height =height;
            return this;
        }

        public Parcel Build()
        {
            //var parcel = new Parcel().GetParcelData(_parcelNumber,_parcelName,_descriptionId,_catergoryId,_parcelWeight,_parcelWidth,_height);
            var parcel = new Parcel();
            return parcel;
        }
    }
}
