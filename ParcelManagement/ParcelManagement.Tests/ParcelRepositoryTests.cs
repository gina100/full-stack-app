﻿using NSubstitute;
using NUnit.Framework;
using System.Collections.Generic;
using ParcelManagement.Interfaces;
using ParcelManagement.Models;

namespace ParcelManagement.Tests
{
    [TestFixture]
    public class ParcelRepositoryTests
    {
        private IParcelRepository parcel;
        private Parcel parcelData;

        [SetUp]
        public void SetUp()
        {
            var builder = new ParcelBuilder(); 
            parcel = Substitute.For<IParcelRepository>();
            parcelData = builder.Build();
        }

        [TestCase]
        public void WhenGettingParcels_ShouldReturnTheEqualNumberOfCapturedParcels()
        {
            //Arrange
            var parcelList = new List<Parcel>() {parcelData,parcelData };
            parcel.GetAllParcels().Returns(parcelList);

            //Act
            var numberOfParcels = parcel.GetAllParcels().Count;

            //Assert
            Assert.AreEqual(2, numberOfParcels);
        }

        [TestCase]
        public void WhenGettingParcel_ShouldReturnCapturedParcel()
        {
            //Arrange
            parcel.GetParcelByParcelKey(parcelData.ParcelNumber).Returns(parcelData);

            //Act
            var returnedParcel = parcel.GetParcelByParcelKey(parcelData.ParcelNumber);

            //Assert
            Assert.AreEqual(parcelData, returnedParcel);
        } 

        [TestCase]
        public void WhenAddignParcel_ShouldReturnTheEqualNumberOfCalls()
        {
            //Assert
            var numberOfCalls = 0;
            parcel.When(x => x.AddParcel(parcelData)).Do(c=>numberOfCalls++);

            //Act
            parcel.AddParcel(parcelData);
            parcel.AddParcel(parcelData);

            //Assert
            Assert.AreEqual(2,numberOfCalls);
        }
        
        [TestCase]
        public void WhenUpdatingParcel_ShouldReturnTheEqualNumberOfCalls()
        {
            //Arrange
            var numberOfCalls = 0;
            parcel.When(x => x.UpdateParcel(parcelData)).Do(c => numberOfCalls++);
          
            //Act
            parcel.UpdateParcel(parcelData);
            parcel.UpdateParcel(parcelData);

            //Assert
            Assert.AreEqual(2,numberOfCalls);
        }
        
        [TestCase]
        public void WhenDeletingParcel_ShouldReturnTheEqualNumberOfCalls()
        {
            //Arrange
            var numberOfCalls = 0;
            parcel.When(c => c.DeleteParcelByParcelKey(parcelData.ParcelNumber)).Do(v => numberOfCalls++);

            //Act
            parcel.DeleteParcelByParcelKey(parcelData.ParcelNumber);
            parcel.DeleteParcelByParcelKey(parcelData.ParcelNumber);

            //Assert
            Assert.AreEqual(2,numberOfCalls);
        }
    }
}
