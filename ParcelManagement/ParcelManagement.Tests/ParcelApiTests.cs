﻿using System.Net.Http;
using System.Threading.Tasks;
using Xunit;
using ParcelManagement.Models;
using System.Collections.Generic;
using System.Text.Json;
using Microsoft.AspNetCore.Mvc.Testing;
using System.Text;
using System.Net;
using System.Linq;

namespace ParcelManagement.Tests
{
    public class ParcelApiTests : IClassFixture<WebApplicationFactory<Startup>>
    {
        private string url;
        private readonly HttpClient client;
        private ParcelBuilder builder;

        public ParcelApiTests(WebApplicationFactory<Startup> factory)
        {
            url = "api/parcel";
            client = factory.CreateClient();
            builder = new ParcelBuilder();
        }
       
        [Fact]
        public async Task WhenGettingParcels_ShouldReturnAllCapturedParcel()
        {
            // Act
            var response = await client.GetAsync(url);
            response.EnsureSuccessStatusCode();
            var stringResponse = await response.Content.ReadAsStringAsync();
            var parcels = JsonSerializer.Deserialize<List<Parcel>>(stringResponse, new JsonSerializerOptions { PropertyNamingPolicy = JsonNamingPolicy.CamelCase });

            //Assert
            Assert.True(0< parcels.Count);
        }

        [Fact]
        public async Task WhenGettingParcel_ShouldReturnCapturedParcel()
        {
            // Arrange
            var parcel = builder.WithParcelNumber(45).Build();
            var request = url+"/GetParcelByParcelNumber/?parcelNumber=" + parcel.ParcelNumber;

            // Act
            var response = await client.GetAsync(request);
            response.EnsureSuccessStatusCode();
            var stringResponse = await response.Content.ReadAsStringAsync();
            var returnedParcel = JsonSerializer.Deserialize<Parcel>(stringResponse, new JsonSerializerOptions { PropertyNamingPolicy = JsonNamingPolicy.CamelCase });

            //Assert
            //Assert.Equal(parcel.ParcelName,returnedParcel.ParcelName);
            //Assert.Equal(parcel.DescriptionKey,returnedParcel.DescriptionKey);
            //Assert.Equal(parcel.CatergoryKey,returnedParcel.CatergoryKey);
            //Assert.Equal(parcel.ParcelWeight,returnedParcel.ParcelWeight);
            //Assert.Equal(parcel.ParcelWidth,returnedParcel.ParcelWidth);
            //Assert.Equal(parcel.Height,returnedParcel.Height);
        }

        [Fact]
        public async Task WhenAddingParcel_ShouldReturnCapturedParcel()
        {
            // Arrange
            var parcel = builder.Build();
            var request = new HttpRequestMessage(HttpMethod.Post, url);
            request.Content = new StringContent(JsonSerializer.Serialize(parcel),Encoding.UTF8,"application/json");

            // Act
            var response = await client.SendAsync(request);
            response.EnsureSuccessStatusCode();
            response = await client.GetAsync(url);
            response.EnsureSuccessStatusCode();
            var stringResponse = await response.Content.ReadAsStringAsync();
            var parcels = JsonSerializer.Deserialize<List<Parcel>>(stringResponse, new JsonSerializerOptions { PropertyNamingPolicy = JsonNamingPolicy.CamelCase });

            //Assert
            //Assert.Equal(parcel.ParcelName, parcels.Last().ParcelName);
            //Assert.Equal(parcel.DescriptionKey, parcels.Last().DescriptionKey);
            //Assert.Equal(parcel.CatergoryKey, parcels.Last().CatergoryKey);
            //Assert.Equal(parcel.ParcelWeight, parcels.Last().ParcelWeight);
            //Assert.Equal(parcel.ParcelWidth, parcels.Last().ParcelWidth);
            //Assert.Equal(parcel.Height, parcels.Last().Height);
        }

        [Fact]
        public async Task WhenUpdatingParcel_ReturnUpdatedParcel()
        {
            // Arrange
            var parcel = builder
                .WithParcelNumber(1)
                .WithParcelName("Samsung S8")
                .WithParcelDescriptionId(1)
                .WithCatergoryId(1)
                .WithParcelWeight(0.2M)
                .WithParcelWidth(1.6M)
                .WithHeight(1.4M)
                .Build();

            var request = new HttpRequestMessage(HttpMethod.Put, url);
            request.Content = new StringContent(JsonSerializer.Serialize(parcel), Encoding.UTF8, "application/json");

            // Act
            var response = await client.SendAsync(request);
            response.EnsureSuccessStatusCode();
            response = await client.GetAsync(url);
            response.EnsureSuccessStatusCode();
            var stringResponse = await response.Content.ReadAsStringAsync();
            var updatedParcel = JsonSerializer.Deserialize<List<Parcel>>(stringResponse, new JsonSerializerOptions { PropertyNamingPolicy = JsonNamingPolicy.CamelCase }).Where(l=>l.ParcelNumber==parcel.ParcelNumber).FirstOrDefault();

            // Assert
            //Assert.Equal(parcel.ParcelName, updatedParcel.ParcelName);
            //Assert.Equal(parcel.DescriptionKey, updatedParcel.DescriptionKey);
            //Assert.Equal(parcel.CatergoryKey, updatedParcel.CatergoryKey);
            //Assert.Equal(parcel.ParcelWeight, updatedParcel.ParcelWeight);
            //Assert.Equal(parcel.ParcelWidth, updatedParcel.ParcelWidth);
            //Assert.Equal(parcel.Height, updatedParcel.Height);
        }
        
        [Fact]
        public async Task WhenDeletingParcel_ShouldReturnParcelNotFound()
        {
            // Add new parcel 
            var parcel = builder.Build();
            var request = new HttpRequestMessage(HttpMethod.Post, url);
            request.Content = new StringContent(JsonSerializer.Serialize(parcel), Encoding.UTF8, "application/json");
            var response = await client.SendAsync(request);
            response.EnsureSuccessStatusCode();
            response = await client.GetAsync(url);

            var stringResponse = await response.Content.ReadAsStringAsync();
            var parcels = JsonSerializer.Deserialize<List<Parcel>>(stringResponse, new JsonSerializerOptions { PropertyNamingPolicy = JsonNamingPolicy.CamelCase });
            parcel.ParcelNumber=parcels.Last().ParcelNumber;
            url = "api/parcel/?parcelNumber=" + parcel.ParcelNumber;
            request = new HttpRequestMessage(HttpMethod.Delete, url);
            request.Content = new StringContent(JsonSerializer.Serialize(parcel), Encoding.UTF8, "application/json");
            response = await client.SendAsync(request);
            response.EnsureSuccessStatusCode();
            url = "api/parcel/GetParcelByParcelNumber/?parcelNumber=" + parcel.ParcelNumber;

            // Act
            response = await client.GetAsync(url);

            //Assert
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }
    }
}
