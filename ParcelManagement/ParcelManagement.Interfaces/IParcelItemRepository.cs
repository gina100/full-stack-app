﻿using ParcelManagement.Models;
using System.Collections.Generic;

namespace ParcelManagement.Interfaces
{
    public interface IParcelItemRepository
    {
        List<ParcelItem> GetAllParcelItems();
        List<ParcelItem> GetAllParcelItemsByParcelKey(string parcelKey);
        ParcelItem GetParcelItemByParcelKeyAndItemKey(string parcelKey,string itemKey);
        void AddParcelItem(ParcelItem item);
        void UpdateParcelItem(ParcelItem item);
    }
}
