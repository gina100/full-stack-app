﻿using ParcelManagement.Models;
using Microsoft.AspNetCore.Mvc;

namespace ParcelManagement.Interfaces
{
    public interface IAccountBusiness
    {
        TokenDetails AuthenticateUser(TokenRequest tokenRequest);
        object GenerateToken(TokenDetails user);
    }
}
