﻿using ParcelManagement.Models;
using System.Collections.Generic;

namespace ParcelManagement.Interfaces
{
    public interface IItemCatergoryRepository
    {
        List<ItemCatergory> GetAllItemCatergories();
    }
}
