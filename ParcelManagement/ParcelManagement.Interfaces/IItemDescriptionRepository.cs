﻿using ParcelManagement.Models;
using System.Collections.Generic;

namespace ParcelManagement.Interfaces
{
    public interface IItemDescriptionRepository
    {
        List<ItemDescription> GetAllItemDescriptions();
    }
}
