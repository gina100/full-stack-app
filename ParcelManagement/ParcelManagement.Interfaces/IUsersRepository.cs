﻿using ParcelManagement.Models;
using System.Collections.Generic;

namespace ParcelManagement.Interfaces
{
    public interface IUsersRepository
    {
        List<UserAddresses> GetAllUsers();
        void AddUser(Users item);
        void UpdateUser(Users item);
        void DeleteUserByKey(string UserKey);
        Users GetUserByKey(string UserKey);
        Users GetUserByCredentails(string userName);

    }
}
