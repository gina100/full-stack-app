﻿using ParcelManagement.Models;
using System.Collections.Generic;

namespace ParcelManagement.Interfaces
{
    public interface IItemRepository
    {
        List<Item> GetAllItems();
        void AddItem(Item item);
        void UpdateItem(Item item);
        void DeleteItemByItemKey(string itemKey);
        Item GetItemByItemKey(string itemKey);

    }
}
