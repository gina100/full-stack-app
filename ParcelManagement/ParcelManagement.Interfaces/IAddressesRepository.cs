﻿using ParcelManagement.Models;
using System.Collections.Generic;

namespace ParcelManagement.Interfaces
{
    public interface IAddressesRepository
    {
        List<Addresses> GetAllAddresses();
        Addresses AddAddress(Addresses item);
        void UpdateAddress(Addresses item);
        void DeleteAddressByKey (string addressKey);
        Addresses GetAddressByKey(string addressKey);
    }
}
