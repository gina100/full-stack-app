﻿using ParcelManagement.Models;
using System.Collections.Generic;

namespace ParcelManagement.Interfaces
{
    public interface IRolesRepository
    {
        List<Roles> GetAllRoles();
        List<Roles> GetAllRolez();
        List<UserRoles> GetAllUserRolesByUserKey(string userKey);
        Roles GetRolesByRoleKey(string roleKey);
        List<UserRoles> GetUserRoles();
        void AddRoles(Roles item);
        void AddUserRoles(UserRoles item);

    }
}
