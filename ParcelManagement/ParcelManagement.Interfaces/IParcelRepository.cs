﻿using ParcelManagement.Models;
using System.Collections.Generic;

namespace ParcelManagement.Interfaces
{
    public interface IParcelRepository
    {
        List<Parcel> GetAllParcels();
        void AddParcel(Parcel item);
        void UpdateParcel(Parcel item);
        void DeleteParcelByParcelKey(string parcelNumber);
        Parcel GetParcelByParcelKey(string parcelNumber);

    }
}
