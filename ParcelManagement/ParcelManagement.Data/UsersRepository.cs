﻿using ParcelManagement.Models;
using ParcelManagement.Interfaces;
using System.Linq;
using Npgsql;
using Dapper;
using System.Data;
using System.Collections.Generic;
using System;

namespace ParcelManagement.Data
{
    public class UsersRepository : IUsersRepository
    { 
        private string connectionString;
        public UsersRepository(string appConnectionString)
        {
            connectionString = appConnectionString;
        }

        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(connectionString);
            }
        }

        public List<UserAddresses> GetAllUsers()
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                var queryResults = dbConnection.Query<UserAddresses>("select * from userslist").ToList();
                dbConnection.Close();

                return queryResults;
            }
        }

        public Users GetUserByKey(string userKey)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                var results = dbConnection.QueryFirstOrDefault<Users>("select * from get_user_by_userkey(@UserKey)", new { userKey= userKey});
                dbConnection.Close();

                return results;
            }
        }
        public Users GetUserByCredentails(string userName)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                var results = dbConnection.QueryFirstOrDefault<Users>("select * from get_user_by_username(@UserName)", new { userName= userName});
                dbConnection.Close();

                return results;
            }
        }

        public void AddUser(Users item)
        {
            item.UserKey= Guid.NewGuid().ToString();
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("call insert_user(@UserKey,@UserName,@UserPassword,@FirstName,@Email,@AddressKey,@LockOut)", item);
                dbConnection.Close();
            }
        }

        public void UpdateUser(Users item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Query("call update_user(@UserKey,@UserName,@UserPassword,@FirstName,@Email,@AddressKey,@LockOut)", item);
                dbConnection.Close();
            }
        }

        public void DeleteUserByKey(string userKey)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("call delete_user(@UserKey)", new { UserKey= userKey});
                dbConnection.Close();
            }
        }
    }
}
