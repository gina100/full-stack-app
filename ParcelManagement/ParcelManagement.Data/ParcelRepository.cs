﻿using ParcelManagement.Models;
using ParcelManagement.Interfaces;
using System.Linq;
using Npgsql;
using Dapper;
using System.Data;
using System.Collections.Generic;
using System;

namespace ParcelManagement.Data
{
    public class ParcelRepository : IParcelRepository
    { 
        private string connectionString;
        public ParcelRepository(string appConnectionString)
        {
            connectionString = appConnectionString;
        }

        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(connectionString);
            }
        }

        public List<Parcel> GetAllParcels()
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                var queryResults = dbConnection.Query<Parcel>("select * from get_all_parcels()").ToList();
                dbConnection.Close();
                return queryResults;
            }
        }

        public Parcel GetParcelByParcelKey(string parcelKey)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                var results = dbConnection.QueryFirstOrDefault<Parcel>("select * from get_parcel_by_parcelkey(@ParcelKey)", new { parcelKey = parcelKey });
                dbConnection.Close();
                return results;
            }
        }

        public void AddParcel(Parcel item)
        {
            item.ParcelKey= Guid.NewGuid().ToString();
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("call insert_parcel(@ParcelKey,@ParcelNumber,@UserKey,@PreferredDropOffAddress,@DateCreated)", item);
                dbConnection.Close();
            }

        }

        public void UpdateParcel(Parcel item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Query("call update_parcel(@ParcelKey,@ParcelNumber,@UserKey,@PreferredDropOffAddress,@DateCreated)", item);
                dbConnection.Close();
            }
        }

        public void DeleteParcelByParcelKey(string parcelKey)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("call delete_parcel(@ParcelKey)", new { parcelKey = parcelKey});
                dbConnection.Close();
            }
        }
    }
}
