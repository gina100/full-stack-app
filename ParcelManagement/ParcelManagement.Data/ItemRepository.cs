﻿using ParcelManagement.Models;
using ParcelManagement.Interfaces;
using System.Linq;
using Npgsql;
using Dapper;
using System.Data;
using System.Collections.Generic;
using System;

namespace ParcelManagement.Data
{
    public class ItemRepository : IItemRepository
    { 
        private string connectionString;
        public ItemRepository(string appConnectionString)
        {
            connectionString = appConnectionString;
        }

        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(connectionString);
            }
        }

        public List<Item> GetAllItems()
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                var queryResults = dbConnection.Query<Item>("select * from itemslist").ToList();
                dbConnection.Close();

                return queryResults;
            }
        }

        public Item GetItemByItemKey(string itemKey)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                var results = dbConnection.QueryFirstOrDefault<Item>("select * from get_item_by_itemkey(@ItemKey);", new { itemKey = itemKey });
                dbConnection.Close();
                return results;
            }
        }

        public void AddItem(Item item)
        {
            item.ItemKey= Guid.NewGuid().ToString();
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("call insert_item(@ItemKey,@ItemName,@ItemNo,@DescriptionKey,@CatergoryKey,@Dimensions,@Price)", item);
                dbConnection.Close();
            }

        }

        public void UpdateItem(Item item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Query("call update_item(@ItemKey,@ItemName,@ItemNo,@DescriptionKey,@CatergoryKey,@Dimensions,@Price)", item);
                dbConnection.Close();
            }
        }

        public void DeleteItemByItemKey(string itemKey)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("call delete_item(@ItemKey)", new { itemKey = itemKey});
                dbConnection.Close();
            }
        }
    }
}
