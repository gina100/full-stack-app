﻿using ParcelManagement.Models;
using ParcelManagement.Interfaces;
using System.Linq;
using Npgsql;
using Dapper;
using System.Data;
using System.Collections.Generic;
using System;

namespace ParcelManagement.Data
{
    public class AddressesRepository : IAddressesRepository
    { 
        private string connectionString;
        public AddressesRepository(string appConnectionString)
        {
            connectionString = appConnectionString;
        }

        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(connectionString);
            }
        }

        public List<Addresses> GetAllAddresses()
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                var queryResults = dbConnection.Query<Addresses>("SELECT * FROM Addresses").ToList();

                return queryResults;
            }
        }

        public Addresses GetAddressByKey(string AddressKey)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                var results = dbConnection.QueryFirstOrDefault<Addresses>("SELECT * FROM Addresses WHERE AddressKey = @AddressKey", new { AddressKey = AddressKey });

                return results;
            }
        }

        public Addresses AddAddress(Addresses item)
        {
            item.AddressKey = Guid.NewGuid().ToString();
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("INSERT INTO Addresses (AddressKey,CityName,Address,AddressRegion,AddressPostalCode) VALUES(@addressKey,@CityName,@Address,@AddressRegion,@AddressPostalCode)", item);

                return item;
            }
            
        }

        public void UpdateAddress(Addresses item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Query("UPDATE Addresses SET CityName=@CityName, Address=@Address, AddressRegion=@AddressRegion, AddressPostalCode=@AddressPostalCode WHERE AddressKey=@AddressKey", item);
            }
        }

        public void DeleteAddressByKey(string AddressKey )
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("DELETE FROM Addressses WHERE AddressKey=@AddressKey", new { AddressKey= AddressKey });
            }
        }
    }
}
