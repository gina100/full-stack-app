﻿using ParcelManagement.Models;
using ParcelManagement.Interfaces;
using System.Linq;
using Npgsql;
using Dapper;
using System.Data;
using System.Collections.Generic;
using System;

namespace ParcelManagement.Data
{
    public class RolesRepository : IRolesRepository
    { 
        private string connectionString;
        public RolesRepository(string appConnectionString)
        {
            connectionString = appConnectionString;
        }

        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(connectionString);
            }
        }

        public List<Roles> GetAllRoles()
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                var queryResults = dbConnection.Query<Roles>("select * from get_all_roles()").ToList();
                dbConnection.Close();

                return queryResults;
            }
        }
        public List<Roles> GetAllRolez()
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                var queryResults = dbConnection.Query<Roles>("call get_all_rols()").ToList();
                dbConnection.Close();

                return queryResults;
            }
        }

        public List<UserRoles> GetAllUserRolesByUserKey(string userKey)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                var queryResults = dbConnection.Query<UserRoles>("select * from  get_userroles_by_userkey(@UserKey)", new { userKey = userKey }).ToList();
                dbConnection.Close();

                return queryResults;
            }
        }

        public Roles GetRolesByRoleKey(string roleKey)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                var results = dbConnection.QueryFirstOrDefault<Roles>("select * from get_role_by_rolekey(@roleKey)", new { roleKey= roleKey});
                dbConnection.Close();

                return results;
            }
        }
        public List<UserRoles> GetUserRoles()
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                var results = dbConnection.Query<UserRoles>("select * from get_all_user_roles()").ToList();
                dbConnection.Close();

                return results;
            }
        }

        public void AddRoles(Roles item)
        {
            item.RoleKey= Guid.NewGuid().ToString();
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("call insert_roles(@RoleKey,@RoleName)", item);
                dbConnection.Close();
            }

        }
        public void AddUserRoles(UserRoles item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("call insert_userroles(@UserKey,@RoleKey)", item);
                dbConnection.Close();
            }
        }
    }
}
