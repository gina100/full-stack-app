﻿using ParcelManagement.Models;
using ParcelManagement.Interfaces;
using System.Linq;
using Npgsql;
using Dapper;
using System.Data;
using System.Collections.Generic;

namespace ParcelManagement.Data
{
    public class ParcelItemRepository : IParcelItemRepository
    { 
        private string connectionString;
        public ParcelItemRepository(string appConnectionString)
        {
            connectionString = appConnectionString;
        }

        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(connectionString);
            }
        }

        public List<ParcelItem> GetAllParcelItems()
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                var queryResults = dbConnection.Query<ParcelItem>("SELECT * FROM ParcelItem").ToList();

                return queryResults;
            }
        }

        public List<ParcelItem> GetAllParcelItemsByParcelKey(string parcelKey)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                var queryResults = dbConnection.Query<ParcelItem>("SELECT * FROM ParcelItem WHERE ParcelKey= @ParcelKey", new { parcelKey = parcelKey }).ToList();

                return queryResults;
            }
        }

        public ParcelItem GetParcelItemByParcelKeyAndItemKey(string parcelKey,string itemKey)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                var results = dbConnection.QueryFirstOrDefault<ParcelItem>("SELECT * FROM ParcelItem WHERE ParcelKey= @parcelKey AND ItemKey= @itemKey", new { parcelKey= parcelKey, itemKey=itemKey});

                return results;
            }
        }
        

        public void AddParcelItem(ParcelItem item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Execute("INSERT INTO ParcelItem (ItemKey,ParcelKey,Quantity) VALUES(@ItemKey,@ParcelKey,@Quantity)", item);
            }
        }

        public void UpdateParcelItem(ParcelItem item)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                dbConnection.Query("UPDATE Parcel SET Quantity=@Quantity WHERE ParcelKey= @ParcelKey AND ItemKey= @ItemKey", item);
            }
        }
    }
}
