﻿using ParcelManagement.Models;
using ParcelManagement.Interfaces;
using System.Linq;
using Npgsql;
using Dapper;
using System.Data;
using System.Collections.Generic;

namespace ParcelManagement.Data
{
    public class ItemCatergoryRepository : IItemCatergoryRepository
    { 
        private string connectionString;
        public ItemCatergoryRepository(string appConnectionString)
        {
            connectionString = appConnectionString;
        }

        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(connectionString);
            }
        }

        public List<ItemCatergory> GetAllItemCatergories()
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                var queryResults = dbConnection.Query<ItemCatergory>("select * from get_all_catergories()").ToList();
                dbConnection.Close();

                return queryResults;
            }
        }
    }
}
