﻿using ParcelManagement.Models;
using ParcelManagement.Interfaces;
using System.Linq;
using Npgsql;
using Dapper;
using System.Data;
using System.Collections.Generic;

namespace ParcelManagement.Data
{
    public class ItemDescriptionRepository : IItemDescriptionRepository
    { 
        private string connectionString;
        public ItemDescriptionRepository(string appConnectionString)
        {
            connectionString = appConnectionString;
        }

        internal IDbConnection Connection
        {
            get
            {
                return new NpgsqlConnection(connectionString);
            }
        }

        public List<ItemDescription> GetAllItemDescriptions()
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();
                var queryResults = dbConnection.Query<ItemDescription>("select * from get_all_descriptions()").ToList();
                dbConnection.Close();

                return queryResults;
            }
        }
    }
}
