CREATE OR REPLACE PROCEDURE 
PUBLIC.insert_item(ItemKey varchar,ItemName varchar,ItemNo varchar,DescriptionKey varchar,CatergoryKey varchar,Dimensions varchar,Price numeric) 
LANGUAGE 'sql' 
AS $BODY$
INSERT INTO public.item (ItemKey,ItemName,ItemNo,DescriptionKey,CatergoryKey,Dimensions,Price) 
VALUES($1,$2,$3,$4,$5,$6,$7);
$BODY$;

call insert_item('')
UPDATE Item SET Name = @Name, ItemNo = @ItemNo, DescriptionKey = @DescriptionKey, CatergoryKey = @CatergoryKey, Dimensions = @Dimensions, Price = @Price WHERE ItemKey = @ItemKey"
CREATE OR REPLACE PROCEDURE 
PUBLIC.update_item(ItemKey varchar,ItemName varchar,ItemNo varchar,DescriptionKey varchar,CatergoryKey varchar,Dimensions varchar,Price numeric) 
LANGUAGE 'sql' 
AS $BODY$
UPDATE Item SET ItemName = $2, ItemNo = $3, DescriptionKey = $4, CatergoryKey = $5, Dimensions = $6, Price = $7 WHERE ItemKey = $1;
$BODY$;

DELETE FROM Item WHERE ItemKey = @ItemKey

CREATE OR REPLACE PROCEDURE 
PUBLIC.delete_item(ItemKey varchar) 
LANGUAGE 'sql' 
AS $BODY$
DELETE FROM Item WHERE ItemKey =$1;
$BODY$;

CREATE OR REPLACE FUNCTION 
public.get_all_parcels()
RETURNS  parcel
LANGUAGE 'sql'
AS $BODY$
select * from public.parcel;
$BODY$;

select * from get_all_parcels()
select * from get_parcel_by_parcelkey('')

CREATE OR REPLACE FUNCTION public.get_parcel_by_parcelkey(
	parcelkey varchar)
    RETURNS parcel
    LANGUAGE 'sql'
AS $BODY$
SELECT * FROM parcel WHERE parcel.parcelKey = $1
$BODY$;
INSERT INTO Parcel (ParcelKey,ParcelName,ParcelNumber,UserKey,PreferedDropOffAddress,DateCreated) 
VALUES(@ParcelKey,@ParcelName,@ParcelNumber,@UserKey,@PreferedDropOffAddress,@DateCreated)

CREATE OR REPLACE PROCEDURE 
PUBLIC.insert_parcel(ParcelKey varchar,ParcelNumber varchar,UserKey varchar,PreferredDropOffAddress varchar,DateCreated date) 
LANGUAGE 'sql' 
AS $BODY$
INSERT INTO public.Parcel (ParcelKey,ParcelNumber,UserKey,PreferredDropOffAddress,DateCreated)  
VALUES($1,$2,$3,$4,$5);
$BODY$;


CREATE OR REPLACE PROCEDURE 
PUBLIC.update_parcel(ParcelKey varchar,ParcelNumber varchar,UserKey varchar,PreferredDropOffAddress varchar,DateCreated date) 
LANGUAGE 'sql' 
AS $BODY$
update public.Parcel set ParcelNumber=$2,UserKey=$3,PreferredDropOffAddress=$4,DateCreated=$5 where parcel.parcelkey=$1;
$BODY$;

CREATE OR REPLACE PROCEDURE 
PUBLIC.delete_parcel(parcelKey varchar) 
LANGUAGE 'sql' 
AS $BODY$
DELETE FROM parcel WHERE parcelKey =$1;
$BODY$;

CREATE OR REPLACE FUNCTION 
public.get_all_catergories()
RETURNS  itemcatergory
LANGUAGE 'sql'
AS $BODY$
select * from public.itemcatergory;
$BODY$;

select * from get_all_catergories()

CREATE OR REPLACE FUNCTION 
public.get_all_descriptions()
RETURNS  itemdescription
LANGUAGE 'sql'
AS $BODY$
select * from public.itemdescription;
$BODY$;

select * from get_all_descriptions()

CREATE OR REPLACE FUNCTION 
public.get_all_roles()
RETURNS  roles
LANGUAGE 'sql'
AS $BODY$
select * from public.roles;
$BODY$;

select * from get_all_roles()

CREATE OR REPLACE FUNCTION public.get_userroles_by_userkey(
	userkey varchar)
    RETURNS userrole
    LANGUAGE 'sql'
AS $BODY$
SELECT * FROM userrole WHERE userrole.userkey = $1
$BODY$;

select * from get_userroles_by_userkey('')

CREATE OR REPLACE FUNCTION public.get_role_by_rolekey(
	rolekey varchar)
    RETURNS roles
    LANGUAGE 'sql'
AS $BODY$
SELECT * FROM roles WHERE roles.rolekey = $1
$BODY$;

CREATE OR REPLACE FUNCTION public.get_all_user_roles()
    RETURNS userrole
    LANGUAGE 'sql'
AS $BODY$
SELECT * FROM userrole
$BODY$;

select * from get_all_user_roles()

INSERT INTO Roles (RoleKey,RoleName) VALUES(@RoleKey,@RoleName)

CREATE OR REPLACE PROCEDURE 
PUBLIC.insert_roles(rolekey varchar,rolename varchar) 
LANGUAGE 'sql' 
AS $BODY$
INSERT INTO public.roles (rolekey,rolename) 
VALUES($1,$2);
$BODY$;

CREATE OR REPLACE PROCEDURE 
PUBLIC.insert_userroles(userkey varchar,rolekey varchar) 
LANGUAGE 'sql' 
AS $BODY$
INSERT INTO public.userrole (userkey,rolekey) 
VALUES($1,$2);
$BODY$;

CREATE OR REPLACE FUNCTION public.get_user_by_userkey(
	userkey varchar)
    RETURNS users
    LANGUAGE 'sql'
AS $BODY$
SELECT * FROM users WHERE users.userKey = $1
$BODY$;

CREATE OR REPLACE FUNCTION public.get_user_by_username(
	username varchar)
    RETURNS users
    LANGUAGE 'sql'
AS $BODY$
SELECT * FROM users WHERE users.username = $1
$BODY$;

INSERT INTO Users (UserKey,UserName,UserPassword,FirstName,Email,AddressKey,LockOut)

CREATE OR REPLACE PROCEDURE 
PUBLIC.insert_user(userKey varchar,username varchar,userpassword varchar,fistname varchar,email varchar,addresskey varchar, lockout bool) 
LANGUAGE 'sql' 
AS $BODY$
INSERT INTO public.Users (UserKey,UserName,UserPassword,FirstName,Email,AddressKey,LockOut)  
VALUES($1,$2,$3,$4,$5,$6,$7);
$BODY$;

CREATE OR REPLACE PROCEDURE 
PUBLIC.update_user(userKey varchar,username varchar,userpassword varchar,fistname varchar,email varchar,addresskey varchar, lockout bool) 
LANGUAGE 'sql' 
AS $BODY$
UPDATE Users SET UserName=$2,UserPassword=$3, FirstName=$4 , Email=$5, AddressKey=$6, LockOut=$7 where users.userkey=$1;
$BODY$;

CREATE OR REPLACE PROCEDURE 
PUBLIC.delete_user(userkey varchar) 
LANGUAGE 'sql' 
AS $BODY$
DELETE FROM users WHERE userKey =$1;
$BODY$;